#!/bin/bash

# Get the directory where the script is located
#script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Change the current working directory to the script's directory
cd $( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
cd "petrotec"
# Now you are in the script's directory
echo "Changed directory to:" . $(pwd)
