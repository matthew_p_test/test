# Test Website - Petrotec
This is an unzipped version of Petrotec from the customer-websites repository.

Version: https://gitlab.wsihq.net/serverless/testing/customer-websites/-/commit/e9ab18303b9081567e2d87d9b174b87ff41fd632

To deploy to Webslice:
1. Create a new website.
    - PHP Runtime: 7.2
    - Document Root: petrotec
1. Create a new database and link it
1. Copy the external connection link from the database details page
1. Uncompress sql dump using `gzip -d db_dump/database.sql.gz`
1. Import database.sql into your database using `pv db_dump/database.sql | mysql -h <EXTERNAL CONNECTION> -u <USERNAME> -p <DATABASE NAME>` then enter the password.
1. In the "Variables" tab, set the following environment variables:
    - ADMIN_PASS to the password you wish to log in to the "admin" user
    - DB_NAME to the name of the created database
    - DB_USER to the username of the created database
    - DB_PASS to the password of the created database
    - DB_HOST to `localhost`
1. In the "Deploys" tab select Git Deploys and give it the URL to this repo, following the instructions
1. Trigger the Initial Git Deploy