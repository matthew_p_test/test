var gulp = require('gulp')
var less = require('gulp-less')
var path = require('path')
var plumber = require('gulp-plumber')
var concat = require('gulp-concat')
var sass = require('gulp-sass')

gulp.task('watch', function() {
  gulp.watch([
    './themes/petrotec/js/*.js',
    './app/js/app.js'
  ],
    ['build-js-app'])
})

gulp.task('sass', function () {
  return gulp.src(
      './themes/petrotec/scss/theme.scss'
    ).pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./themes/petrotec/dist'));
});

gulp.task('sass:watch', function () {
  gulp.watch('./themes/**/*.scss', ['sass']);
});

gulp.task('build-js-app', function() {
  return gulp.src([
      './themes/petrotec/js/jquery.min.js',
      './themes/petrotec/js/jquery.plugin.min.js',
      './themes/petrotec/js/bootstrap.min.js',
      './themes/petrotec/js/jquery.flexslider-min.js',
      './themes/petrotec/js/smooth-scroll.min.js',
      './themes/petrotec/js/skrollr.min.js',
      './themes/petrotec/js/scrollReveal.min.js',
      './themes/petrotec/js/isotope.min.js',
      './themes/petrotec/js/lightbox.min.js',
      './themes/petrotec/js/jquery.countdown.min.js',
      './themes/petrotec/js/scripts.js',
      './app/js/app.js'
    ])
    .pipe(concat('app.min.js'))
    .pipe(gulp.dest('./app/dist'));
})

gulp.task('default', ['sass','build-js-app', 'sass:watch', 'watch'])
