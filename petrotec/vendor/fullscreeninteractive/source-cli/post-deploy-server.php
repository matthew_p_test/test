#!/usr/local/bin/php
<?php

echo "Running go-live operations .";
echo exec("cd /container/application/ && rm -rf ./silverstripe-cache && mkdir ./silverstripe-cache");
echo ".";

$root = dirname(dirname(dirname(dirname(__FILE__))));
require_once trim($root) .'/vendor/autoload.php';

$cmd = sprintf(
    'php %s dev/build',
    dirname(__FILE__) .'/cli-script.php'
);

echo PHP_EOL . "Running ". $cmd . PHP_EOL;

while (@ob_end_flush()) {
// end all output buffers if any
}
$proc = popen($cmd, 'r');

while (!feof($proc)) {
    echo fread($proc, 4096);
    @flush();
}

echo "\xE2\x9C\x85\033[37m dev/build done. \033[0m" .PHP_EOL;

// check for the health
try {
    $dotenv = Dotenv\Dotenv::createImmutable($root);
    $dotenv->load();
} catch (Exception $e) {

}

if (!defined('SS_BASE_URL')) {
    try {
        $dotenv = Dotenv\Dotenv::createImmutable(dirname($root));
        $dotenv->load();
    }
    catch (Exception $e) {

    }
}

if (defined('SS_BASE_URL')) {
    $base = getenv('SS_BASE_URL');
    echo exec('curl -s '. $base. '/dev/check');

    echo PHP_EOL;
    echo "\xE2\x9C\x85\033[37m web flush done. \033[0m" .PHP_EOL;
}

echo "\xE2\x9C\x85\033[37m post-deploy-server done. \033[0m" .PHP_EOL;
