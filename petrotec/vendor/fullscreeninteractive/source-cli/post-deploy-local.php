#!/usr/local/bin/php
<?php

$root = dirname(dirname(dirname(dirname(__FILE__))));

function srcPath($path)
{
    global $root;

    return $root . DIRECTORY_SEPARATOR . $path;
}

require_once srcPath('vendor/autoload.php');

$dotenv = \Dotenv\Dotenv::createImmutable($root, '.source_env');
$dotenv->load();

$hook = getenv('SENTRY_DEPLOY_HOOK');
$who = `whoami`;
$project = getenv('PROJECT_NAME');

$logFile = srcPath('.beamlog');

if (file_exists($logFile)) {
    $contents = file_get_contents($logFile);
} else {
    $contents = '';
}

if ($hook) {
    $lines = explode(PHP_EOL, $contents);
    $revision = substr($lines[2], strlen('commit '));

    exec("curl $hook -X POST -H 'Content-Type: application/json' -d '{\"version\": \"$revision\"}' --silent");
    echo "\xE2\x9C\x85\033[37m sentry hook done. \033[0m" .PHP_EOL;
} else {
    echo "\xE2\x9D\x8C Sentry hook skipped".PHP_EOL;
}


$slack = getenv('SLACK_HOOK');
$channel = ($channel = getenv('SLACK_CHANNEL')) ? $channel : 'deployments';

if ($slack) {
    $message = sprintf("%s", trim($contents));
    $slackCmd = "curl -X POST -H 'Content-type: application/json' --data '{\"text\":\"$message\", \"icon_emoji\": \":alien:\", \"username\": \"Source Deployer(tm)\", \"channel\":\"$channel\"}' $slack  --silent";

    $result = exec($slackCmd);

    echo "\xE2\x9C\x85\033[37m slack done. \033[0m" .PHP_EOL;
} else {
    echo "\xE2\x9D\x8C Slack skipped". PHP_EOL;
}

echo "\xE2\x9C\x85\033[37m post-deploy-local done. \033[0m" .PHP_EOL;
