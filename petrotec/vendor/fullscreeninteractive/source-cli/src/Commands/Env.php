<?php

namespace FullscreenInteractive\Source\Bin\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Env extends SourceCommand
{
    protected $directional = false;

    protected $environmental = true;

    protected function configure()
    {
        $this->setName('env');
        $this->setDescription('Env operations: (test|prod)');
        $this->addArgument('environment', InputArgument::REQUIRED, 'Select an environment (test|prod)');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        parent::execute($input, $output);

        $this->outputEnv($input, $output);

        return 0;
    }

    /**
     * Outputs environment values
     *
     */
    protected function outputEnv($input, $output)
    {
        $process = $this->runSSH(sprintf(
            'cat /container/application/.env'
        ));

        echo $process->getOutput();

        $this->outputDone();
    }
}
