<?php

namespace FullscreenInteractive\Source\Bin\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Db extends SourceCommand
{
    protected $directional = true;

    protected $environmental = true;

    protected function configure()
    {
        $this->setName('db');
        $this->setDescription('Database operations: (down|up) (test|prod)');
        $this->addArgument('direction', InputArgument::REQUIRED, 'Select a direction (down|up)');
        $this->addArgument('environment', InputArgument::REQUIRED, 'Select an environment (test|prod)');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        parent::execute($input, $output);

        if ($this->isUp()) {
            // sync database up.
            $this->syncDatabaseUp($input, $output);
        } else {
            $this->syncDatabaseDown($input, $output);
        }

        return 0;
    }

    /**
     * Pulls a remote database down into the local environment
     *
     */
    protected function syncDatabaseDown($input, $output)
    {
        $localDB = getenv('DB_LOCAL');

        $fileName = 'source-db-'. $localDB . '.sql';

        $this->outputStage(sprintf('Exporting <info>%s</info> from <info>%s</info>', $this->envVars['db'], $this->envVars['ssh']));

        $this->runSSH(sprintf(
            'mysqldump %s > %s',
            $this->envVars['db'],
            $this->getRemoteCachePath($fileName)
        ));

        $this->outputStage('Retrieving database export from server');
        $this->fetchFile($this->getRemoteCachePath($fileName), $this->getLocalCachePath($fileName));

        $this->outputStage(sprintf('Importing remote database into local <info>%s</info>', $localDB));

        $this->runLocalMysql(sprintf('CREATE DATABASE IF NOT EXISTS `%s`', $localDB));

        $this->runLocal(sprintf(
            'mysql %s < %s',
            $localDB,
            $this->getLocalCachePath($fileName)
        ));

        $this->outputDone();
    }

    /**
     * Uploads a local database to a remote environment. This will take a backup
     * of the database.
     *
     */

    protected function syncDatabaseUp($input, $output)
    {
        $localDB = getenv('DB_LOCAL');
        $fileName = 'source-live-backup-'. $this->envVars['db'] . '.sql';

        $this->outputStage(sprintf('Backing up <info>%s</info> from <info>%s</info>', $this->envVars['db'], $this->envVars['ssh']));

        $this->runSSH(sprintf(
            'mysqldump %s > %s',
            $this->envVars['db'],
            $this->getRemoteCachePath($fileName)
        ));

        $this->outputCompleted(sprintf('mysql successfully backed up to %s', $this->getRemoteCachePath($fileName)));

        $localFile = 'local-db-export-'. $localDB.'.sql';
        $this->outputStage(sprintf('Exporting local database <info>%s</info>', $localFile));

        $this->runLocal(sprintf('mysqldump %s > /tmp/%s', $localDB, $localFile));

        $this->outputStage(sprintf('Uploading local database <info>%s</info> via rsync', $localDB));
        $this->uploadFile("/tmp/$localFile", $this->getRemoteCachePath($localFile));
        $this->outputCompleted(sprintf('Successfully uploaded in %s seconds', $this->endTimer()));

        $this->outputStage(sprintf('Importing into <info>%s</info> from %s', $this->envVars['db'], $localFile));
        $this->runSSH(sprintf(
            'mysql %s < %s',
            $this->envVars['db'],
            $this->getRemoteCachePath($localFile)
        ));

        $this->outputDone();
    }
}
