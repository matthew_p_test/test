<?php

namespace FullscreenInteractive\Source\Bin\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Assets extends SourceCommand
{
    protected function configure()
    {
        $this->setName('assets');
        $this->setDescription('Assets operations: (down|up) (test|prod)');
        $this->addArgument('direction', InputArgument::REQUIRED, 'Select a direction (down|up)');
        $this->addArgument('environment', InputArgument::REQUIRED, 'Select an environment (test|prod)');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        parent::execute($input, $output);

        if ($this->isUp()) {
            // sync database up.
            $this->syncAssetsUp($input, $output);
        } else {
            $this->syncAssetsDown($input, $output);
        }

        return 0;
    }

    protected function getRemoteAssetsPath()
    {
        $beam = json_decode(file_get_contents($this->getBaseDir() . '/beam.json'), true);
        $remote = $beam["servers"][strtolower($this->env)]["webroot"];

        // check to see if the local uses a 'public' folder. If so we change it.
        if (strpos($this->getLocalAssetsPath(), '/public/assets') !== false) {
            $remote .= (substr($remote, -1) == '/') ? 'public/assets/' : '/public/assets/';
        } else {
            $remote .= (substr($remote, -1) == '/') ? 'assets/' : '/assets/';
        }

        return $remote;
    }

    protected function getLocalAssetsPath()
    {
        $base = $this->getBaseDir();
        $candidate = $base .'/assets';

        if (is_dir($candidate)) {
            return $candidate;
        } elseif (is_dir("$base/public/assets")) {
            return "$base/public/assets";
        }

        return false;
    }

    protected function syncAssetsDown($input, $output)
    {
        $this->outputStage(sprintf('Syncing assets down'));
        $this->fetchFile($this->getRemoteAssetsPath(), $this->getLocalAssetsPath());
        $this->runLocal(sprintf('chmod -R 777 %s', $this->getLocalAssetsPath()));

        $this->outputDone();
    }

    protected function syncAssetsUp($input, $output)
    {
        $this->outputStage(sprintf('Syncing assets up'));
        $this->runLocal(sprintf('chmod -R 777 %s', $this->getLocalAssetsPath()));

        $this->uploadFolder($this->getLocalAssetsPath(), dirname($this->getRemoteAssetsPath()));
        $this->outputDone();
    }
}
