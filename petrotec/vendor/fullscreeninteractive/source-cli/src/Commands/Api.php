<?php

namespace FullscreenInteractive\Source\Bin\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Api extends SourceCommand
{
    protected function configure()
    {
        $this->setName('api');
        $this->setDescription('Make a call to the underlying API');
        $this->addArgument('call', InputArgument::REQUIRED, 'API route to call');
        $this->addOption('user', null, InputOption::VALUE_REQUIRED, 'memberId to make the call');
        $this->addOption('data', null, InputOption::VALUE_REQUIRED, 'Variable input string');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('You called the API');

        return 0;
    }
}
