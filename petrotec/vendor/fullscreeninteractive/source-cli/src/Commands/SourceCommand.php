<?php


namespace FullscreenInteractive\Source\Bin\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class SourceCommand extends Command
{
    const ENV_TEST = 'TEST';

    const ENV_PROD = 'PROD';

    const WAY_UP = 'UP';

    const WAY_DOWN = 'DOWN';

    protected $env;

    protected $way;

    protected $io;

    protected $directional = true;

    protected $environmental = true;

    protected $envVars = [];

    protected $timer = null;

    /**
     * @todo use pwd.
     */
    protected function getBaseDir()
    {
        return dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))));
    }
    /**
     * Returns the full remote cache path for a given file.
     *
     *
     * @param string $fileName
     *
     * @return string
     */
    protected function getRemoteCachePath($fileName = '')
    {
        return '~/.source/'. $fileName;
    }

    /**
     * @return string
     */
    protected function getRemoteBasePath()
    {
        $beam = json_decode(file_get_contents($this->getBaseDir() . '/beam.json'), true);
        $remote = rtrim($beam["servers"][strtolower($this->env)]["webroot"], '/') .'/';

        return $remote;
    }

    /**
     * Returns the full local cache path for a given file.
     *
     *
     * @param string $fileName
     *
     * @return string
     */
    protected function getLocalCachePath($fileName = '')
    {
        return '/tmp/'. $fileName;
    }

    /**
     * Ensures the full remote cache path exists on the server.
     *
     * @throws Exception
     * @return void
     */
    protected function ensureSSHCache()
    {
        $this->runSSH(sprintf('mkdir -p %s', $this->getRemoteCachePath()));
    }

    /**
     * Runs a command on the remote server
     *
     *
     * @param string $command
     * @throws ProcessFailedException
     *
     * @return Process
     */
    protected function runSSH($command)
    {
        $server = $this->envVars['ssh'];

        if (!$server) {
            throw new \Exception('Missing ssh host for "'.$this->env.'"');
        }

        $process = new Process(["ssh", $server, $command]);
        $process->setTimeout(null);
        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        return $process;
    }

    /**
     * Runs a command on the local server.
     *
     *
     * @param array|string $command
     * @throws ProcessFailedException
     *
     * @return Process
     */
    protected function runLocal($command)
    {
        if (is_array($command)) {
            $process = new Process($command);
        } else {
            $process = Process::fromShellCommandline($command);
        }

        $process->setTimeout(null);
        $process->mustRun();

        return $process;
    }

    /**
     * Runs a command on the remote server
     *
     *
     * @param string $remotePath
     * @param string $localPath
     *
     * @throws ProcessFailedException
     *
     * @return Process
     */
    protected function fetchFile($remote, $local)
    {
        $process = Process::fromShellCommandline(sprintf(
            "rsync -rv --progress %s:%s %s",
            $this->envVars['ssh'],
            $remote,
            $local
        ));

        $process->setTimeout(null);
        $process->mustRun();

        return $process;
    }

    /**
     * Uploads a file to the remote server
     *
     * @param string $localPath
     * @param string $remotePath
     *
     * @throws ProcessFailedException
     *
     * @return Process
     */
    protected function uploadFolder($localFolder, $remoteFolder)
    {
        $process = Process::fromShellCommandline(sprintf(
            "rsync -rv --delete --progress %s %s:%s",
            $localFolder,
            $this->envVars['ssh'],
            $remoteFolder
        ));

        $process->setTimeout(null);
        $process->start();

        foreach ($process as $type => $data) {
            if ($process::OUT === $type) {
                echo $data."\n";
            } else {
                echo "[ERR] ".$data."\n";
            }
        }

        return $process;
    }


    /**
     * Uploads a file to the remote server
     *
     * @param string $localPath
     * @param string $remotePath
     *
     * @throws ProcessFailedException
     *
     * @return Process
     */
    protected function uploadFile($local, $remote)
    {
        $process = Process::fromShellCommandline(sprintf(
            "rsync -rv --delete --progress %s %s:%s",
            $local,
            $this->envVars['ssh'],
            $remote
        ));

        $process->setTimeout(null);
        $process->start();

        foreach ($process as $type => $data) {
            if ($process::OUT === $type) {
                echo $data."\n";
            } else {
                echo "[ERR] ".$data."\n";
            }
        }

        return $process;
    }

    /**
     * Returns true if the script is operating upwards
     *
     * @return bool
     */
    protected function isUp()
    {
        return $this->way === self::WAY_UP;
    }

    /**
     * Returns true if the script is operating downwards
     *
     * @return bool
     */
    protected function isDown()
    {
        return $this->way === self::WAY_DOWN;
    }

    /**
     * Loads the configuration from the environment.
     *
     * @param InputInterface
     * @param OutputInterface
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->loadEnvVars();

        if ($this->environmental) {
            $this->env = strtoupper($input->getArgument('environment'));

            if (!in_array($this->env, [self::ENV_TEST, self::ENV_PROD])) {
                throw new \Exception('Invalid environment provided');
            }

            $this->envVars = [
                'ssh' => getenv('SSH_'. $this->env),
                'db' => getenv('DB_'. $this->env)
            ];
        }

        if ($this->directional) {
            $this->way = strtoupper($input->getArgument('direction'));

            // support aliases of up and down with get and push.
            if ($this->way === 'GET') {
                $this->way = self::WAY_DOWN;
            } elseif ($this->way === 'PUSH') {
                $this->way = self::WAY_UP;
            }

            if (!in_array($this->way, [self::WAY_UP, self::WAY_DOWN])) {
                throw new \Exception('Invalid direction provided');
            }
        }

        $this->io = new SymfonyStyle($input, $output);

        $this->ensureSSHCache();
    }

    protected function loadEnvVars()
    {
        $root = dirname(dirname(dirname(dirname(dirname(__DIR__)))));
        $dotenv = \Dotenv\Dotenv::createImmutable($root, '.source_env');
        $dotenv->load();
    }

    /**
     * Runs a MySQL command with the users local permissions
     *
     * @param string
     *
     * @return Process
     */
    protected function runLocalMySQL($command)
    {
        $user = `whoami`;
        $process = Process::fromShellCommandline(sprintf("mysql --defaults-file=/Users/%s/.my.cnf -e '%s'", trim($user), $command));
        $process->mustRun();

        return $process;
    }

    /**
     * Output task heading.
     *
     * @param string
     *
     * @return void
     */
    protected function outputStage($string)
    {
        $this->io->text(sprintf("<comment>[%s]</comment> %s", date('Y-m-d H:i:s'), $string));
    }

    /**
     *
     */
    protected function outputSection($string)
    {
        $this->io->section($string);
    }

    /**
     * Output completed line.
     *
     * @param string
     *
     * @return void
     */
    protected function outputCompleted($string)
    {
        $this->io->text(sprintf("<comment>[%s]</comment> \xE2\x9C\x85  %s", date('Y-m-d H:i:s'), $string));
    }

    /**
     *
     * @param string
     *
     * @return void
     */
    protected function outputDone($message = 'Done')
    {
        $this->io->text("\xE2\x9C\x85   " . $message);
    }

    /**
     * Start a timer
     *
     */
    protected function startTimer()
    {
        $this->timer = time();

        return $this;
    }

    /**
     *
     */
    protected function endTimer()
    {
        if (!$this->timer) {
            return null;
        }

        return number_format(time() - $this->timer, 2);
    }
}
