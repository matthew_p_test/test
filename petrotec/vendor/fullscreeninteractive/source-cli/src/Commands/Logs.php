<?php

namespace FullscreenInteractive\Source\Bin\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;

class Logs extends SourceCommand
{
    protected function configure()
    {
        $this->setName('logs');
        $this->setDescription('Retrieves the apache logs from either test or prod');
        $this->setDefinition(new InputDefinition([
            new InputOption('environment', 'e', InputOption::VALUE_REQUIRED),
        ]));
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        parent::execute($input, $output);

        $output->writeln(sprintf('<info>[*] Fetching apache logs</info>'));
        $helper = $this->getHelper('process');

        $process = $this->runSSH('cat /container/logs/apache2/error.log');
        $helper->run($output, $process, 'The process failed :(', function ($type, $data) use ($output) {
            if (Process::ERR === $type) {
                $output->writeln($data);
            } else {
                $output->writeln($data);
            }
        });

        return 0;
    }
}
