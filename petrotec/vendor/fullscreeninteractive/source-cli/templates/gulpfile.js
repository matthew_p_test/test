var gulp = require('gulp')
var less = require('gulp-less')
var path = require('path')
var plumber = require('gulp-plumber')
var concat = require('gulp-concat')

gulp.task('watch', function() {
  gulp.watch([
    './app/less/*.less',
  ], [ 'build-less-app'])
  gulp.watch('./app/js/*.js', ['build-js-app'])
})

gulp.task('build-less-app', function() {
  gulp.src([
      './app/less/app.less'
    ])
    .pipe(plumber())
    .pipe(less({
      paths: [path.join(__dirname, 'less', 'includes')]
    }))
    .pipe(concat('app.min.css'))
    .pipe(gulp.dest('./app/dist'))
})

gulp.task('build-js-app', function() {
  gulp.src([
      './app/bower_components/jquery/dist/jquery.js',
      './app/bower_components/bootstrap/dist/js/bootstrap.min.js',
      './app/js/app.min.js'
    ])
    .pipe(concat('app.js'))
    .pipe(gulp.dest('./app/dist/'));
})

gulp.task('default', ['build-less-app', 'build-js-app', 'watch'])
