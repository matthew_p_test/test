<?php
// This script will copy a pre-commit hook from this repo to a developers local
// git.
//
// The pre-commit hook will be run whenever a developer runs "git commit"
//
// This script will run after running composer install, via the
// "post-install-cmd" in composer.json
//
// "scripts": {
//    "post-install-cmd": [
//        "php ./vendor/govtnz/core/bin/composer-script.php"
//    ],
//  }
$projectRoot = dirname(dirname(dirname(dirname(__DIR__))));
$gitHookFilename = '/.git/hooks/pre-commit';
$shellHookFilename = __DIR__ .'/pre-commit.sh';

if (file_exists($projectRoot . $gitHookFilename)) {
    unlink($projectRoot . $gitHookFilename);
}

copy($shellHookFilename, $projectRoot . $gitHookFilename);
chmod($projectRoot . $gitHookFilename, 0744);
