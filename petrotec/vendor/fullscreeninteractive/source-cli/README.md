```

  ,adPPYba,   ,adPPYba,   88       88  8b,dPPYba,   ,adPPYba,   ,adPPYba,
  I8[    ""  a8"     "8a  88       88  88P'   "Y8  a8"     ""  a8P_____88
   `"Y8ba,   8b       d8  88       88  88          8b          8PP"""""""
  aa    ]8I  "8a,   ,a8"  "8a,   ,a88  88          "8a,   ,aa  "8b,   ,aa
  `"YbbdP"'   `"YbbdP"'    `"YbbdP'Y8  88           `"Ybbd8"'   `"Ybbd8"'

```

Source(tm) is Fullscreen Interactive's opinated framework for building and
running robust web services, applications and mobile application.

This repo contains scripts and helper tasks for dealing with Source projects,
including syncing databases and deployment tracking.

# Environments

* `test`
* `prod`