<p>Hi $Member.Name</p>

<% if Copy %>
    <p>$Copy</p>
<% else %>
    <p>Thanks for signing up to <a href="$BaseHref">$SiteName</a></p>
<% end_if %>

<% if Password %>
    <p>Your Password: <strong>$Password</strong></p>
<% end_if %>

<p>Please confirm your account by clicking on the link below.</p>

<p><a href="confirm/account/$Member.ID/$Member.ConfirmToken">Confirm your account</a></p>

<p>If you have any issues or questions please get in touch with us.</p>
