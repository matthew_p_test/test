var gulp = require('gulp')
var phplint = require('phplint').lint

gulp.task('phplint', function (cb) {
  phplint(['src/**/*.php'], {limit: 10}, function (err, stdout, stderr) {
    if (err) {
      cb(err)
      process.exit(1)
    }
    cb()
  })
})
