```

  ,adPPYba,   ,adPPYba,   88       88  8b,dPPYba,   ,adPPYba,   ,adPPYba,
  I8[    ""  a8"     "8a  88       88  88P'   "Y8  a8"     ""  a8P_____88
   `"Y8ba,   8b       d8  88       88  88          8b          8PP"""""""
  aa    ]8I  "8a,   ,a8"  "8a,   ,a88  88          "8a,   ,aa  "8b,   ,aa
  `"YbbdP"'   `"YbbdP"'    `"YbbdP'Y8  88           `"Ybbd8"'   `"Ybbd8"'

```

Source(tm) is Fullscreen Interactive's opinated framework for building and
running robust web services, applications and mobile application.

This repo contains all the source code and helpers for dealing with SilverStripe
projects. It's designed to provide common functionality for dealing with the
mobile API and functionality such as registration and forgot password.

# Queue Manager

Handles things like notifications

	*/1 * * * * php /container/application/vendor/fullscreeninteractive/source-backend/cli-script.php dev/tasks/RunJobQueueTask

# Root CA Cert

wget https://www.entrust.com/root-certificates/entrust_2048_ca.cer -O - > entrust_root_certification_authority.pem


# Routes

    'legal//$Action': 'FullscreenInteractive\Source\Controllers\LegalController'
    'register//$Action/$ID/$OtherID' : 'FullscreenInteractive\Source\Controllers\RegisterController'
    'download//$Action/$ID/$OtherID' : 'FullscreenInteractive\Source\Controllers\DownloadController'
    'confirm//$Action/$ID/$OtherID': 'FullscreenInteractive\Source\Controllers\ConfirmAccountController'
