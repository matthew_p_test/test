<?php

use SilverStripe\Control\Director;
use SilverStripe\Core\EnvironmentLoader;
use SilverStripe\Core\Environment;

$loader = new EnvironmentLoader();
$path = dirname(dirname(dirname(__DIR__))). "/.source_env";
$result = $loader->loadFile($path);

if ($result === null) {
    throw new Exception("Error Processing Request, cannot read $path", 1);
}

if (Director::isLive() || Director::isTest()) {
    $dsn = Environment::getEnv('SENTRY_DSN');

    if ($dsn) {
        Sentry\init(['dsn' => $dsn]);
    } else {
        throw new Exception("SENTRY_DSN not set in $path", 1);
    }
}

// mobile app token from .source_env
define('MOBILE_APP_TOKEN', Environment::getEnv('MOBILE_APP_TOKEN'));

// can define no SSL. Excludes security ID as some wkhtmltopdf services need to
// use http.
if (!Environment::getEnv('NO_FORCE_SSL') && !Director::isDev()) {
    if (isset($_GET['SecurityID'])) {
        //
    } elseif (!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] !== 'on') {
        if (isset($_SERVER['HTTP_HOST'])) {
            if (!headers_sent()) {
                header("Status: 301 Moved Permanently");
                header(sprintf(
                    'Location: https://%s%s',
                    $_SERVER['HTTP_HOST'],
                    $_SERVER['REQUEST_URI']
                ));
                exit();
            }
        }
    }
}
