<?php

namespace FullscreenInteractive\Source;

use SilverStripe\Control\Director;
use SilverStripe\Security\Security;

class Sms
{

    private static $whitelist = [
        '+64273893454',
        '+64278413223'
    ];

    public static function send($number, $txt)
    {
        $number = self::prepNumber($number);

        if (strpos($number, '+64') !== 0) {
            if (strpos($number, '0') === 0) {
                $number = substr($number, 1);
            }

            $number = '+64'. $number;
        }

        if (!defined('TWILLIO_AUTH') || !defined('TWILLIO_SID')) {
            return false;
        }

        if (Director::isDev()) {
            if (!in_array($number, self::$whitelist)) {
                return false;
            }
        }

        $sid = TWILLIO_SID;
        $token = TWILLIO_AUTH;
        $client = new Twilio\Rest\Client($sid, $token);
        $message = $client->messages->create(
            $number,
            array(
                'from' => '+18552018427',
                'body' => $txt
            )
        );

        return true;
    }

    public static function prepNumber($number)
    {
        return trim(str_replace([' ', '(', ')'], '', $number));
    }
}
