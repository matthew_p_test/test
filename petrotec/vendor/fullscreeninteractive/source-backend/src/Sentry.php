<?php

namespace FullscreenInteractive\Source;

use SilverStripe\Core\Injector\Injector;
use SilverStripe\Control\Director;
use Psr\Log\LoggerInterface;

class Sentry
{
    public static function info($message)
    {
        if (Director::isDev()) {
            echo $message .PHP_EOL;
        }

        Injector::inst()->get(LoggerInterface::class)->info($message);
    }

    public static function log($message)
    {
        if ($message instanceof Exception) {
            return static::logException($message);
        }

        if (Director::isDev()) {
            user_error($message);
            die();
        }

        Injector::inst()->get(LoggerInterface::class)->error($message);
    }

    public static function logException(\Exception $e, $opts = [])
    {
        Injector::inst()->get(LoggerInterface::class)->error($e->getMessage());
    }
}
