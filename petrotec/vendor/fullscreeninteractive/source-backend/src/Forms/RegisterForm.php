<?php

namespace FullscreenInteractive\Source\Forms;

use SilverStripe\Forms\Form;
use SilverStripe\Control\Controller;
use SilverStripe\Control\Director;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\EmailField;
use SilverStripe\Forms\PasswordField;
use SilverStripe\Forms\FormAction;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\RequiredFields;
use SilverStripe\Security\Member;
use SilverStripe\ORM\FieldType\DBField;
use SilverStripe\Forms\HiddenField;
use SilverStripe\Core\Convert;

class RegisterForm extends Form
{
    public function __construct($controller, $name)
    {
        $privacy = Controller::join_links(
            Director::absoluteBaseURL(),
            'legal',
            'privacy-policy'
        );

        $terms = Controller::join_links(
            Director::absoluteBaseURL(),
            'legal',
            'terms-of-service'
        );

        $nextStep = Convert::raw2att($controller->getRequest()->getVar('nextStep'));
        $fields = new FieldList(
            new TextField('FirstName', 'First name'),
            new TextField('Surname', 'Surname'),
            new EmailField('Email', 'Email'),
            new HiddenField('TakeUserToo', '', $nextStep),
            $password = new PasswordField('Password', 'New Password'),
            new CheckboxField(
                'Terms',
                DBField::create_field(
                    'HTMLText',
                    sprintf(
                        'I agree to the <a href="%s" target="_blank">Privacy Policy</a> and <a href="%s" target="_blank">Terms of Conditions</a> for using our website, app and related services',
                        $privacy,
                        $terms
                    )
                )
            )
        );

        $password->setDescription('Make sure your password is secure. Want a random password? Try '. singleton(Member::class)->generatePassword());

        $controller->extend('updateRegisterFields', $fields);

        $actions = new FieldList(
            new FormAction('doRegister', "Register")
        );

        $controller->extend('updateRegisterActions', $actions);

        $validator = new RequiredFields(
            'FirstName',
            'Surname',
            'Email',
            'Password',
            'Terms',
            'DateOfBirth'
        );

        $controller->extend('updateRegisterValidator', $validator);

        parent::__construct($controller, $name, $fields, $actions, $validator);
    }

    public function doRegister($data, $form)
    {
        $existing = Member::get()->filter('Email', trim(strtolower($data['Email'])));

        if ($existing->exists()) {
            $form->sessionMessage(
                'That email address has already been registered. '.
                'Please use another address or login as this account to continue. '.
                'If you cannot remember your password please Reset your Password on the login screen.'
            );
            $form->setSessionData($data);
            $form->setFieldMessage('Email', 'Please use another email address.');

            return $this->controller->redirectBack();
        }

        $member = Member::create();
        $form->saveInto($member);
        $member->write();
        $member->PlainPassword = $data['Password'];

        $this->controller->extend('onAfterRegister', $member, $data);

        $member->sendAccountConfirmation();

        if (isset($data['TakeUserToo']) && $data['TakeUserToo']) {
            return $this->controller->redirect($data['TakeUserToo']);
        } else {
            return $this->controller->redirect('register/thanks');
        }
    }
}
