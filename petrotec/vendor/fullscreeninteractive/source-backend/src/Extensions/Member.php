<?php

namespace FullscreenInteractive\Source\Extensions;

use FullscreenInteractive\Source\Model\Notification;
use SilverStripe\ORM\DataExtension;
use SilverStripe\Forms\FieldList;
use SilverStripe\Control\Email\Email;
use SilverStripe\View\ArrayData;
use SilverStripe\Security\RandomGenerator;
use SilverStripe\Security\Permission;
use SilverStripe\ORM\DB;
use SilverStripe\Core\Config\Config;
use SilverStripe\Control\Controller;
use SilverStripe\Control\Director;

class Member extends DataExtension
{
    private static $db = [
        'ConfirmToken' => 'Varchar(255)',
        'Username' => 'Varchar(255)',
        'ConfirmedAccount' => 'Boolean',
        'MobileSessionToken' => 'Varchar(255)',
        'DisableEmailNotifications' => 'Boolean',
        'DisablePushNotifications' => 'Boolean'
    ];

    private static $has_many = [
        'DeviceTokens' => 'FullscreenInteractive\Source\Model\DeviceToken'
    ];

    private static $has_one = [
        'Photo' => 'SilverStripe\Assets\Image'
    ];

    /**
     *
     */
    public function updateCMSFields(FieldList $fields)
    {
        $fields->removeByName('DeviceTokens');
        $fields->removeByName('MobileSessionToken');
        $fields->removeByName('ConfirmToken');

        if (!Permission::check('ADMIN')) {
            $fields->makeFieldReadonly('ConfirmedAccount');
            $fields->makeFieldReadonly('Username');
        }
    }

    /**
     *
     */
    public function onBeforeWrite()
    {
        if (!$this->owner->Username) {
            $this->owner->Username = $this->owner->Email;
        }

        $generator = new RandomGenerator();

        if (!$this->owner->ConfirmToken && !$this->owner->ConfirmedAccount) {
            $this->owner->ConfirmToken = $generator->randomToken('sha1');
        }
    }

    /**
     *
     */
    public function generateUserName()
    {
        $this->owner->Username = $this->owner->Email;

        $existing = \SilverStripe\Security\Member::get()
            ->filter('Username', $this->owner->username)
            ->exclude('ID', $this->owner->ID);

        while ($existing->exists()) {
            $this->owner->Username = $this->owner->Email. '-'. rand(1000, 9999);

            $existing = \SilverStripe\Security\Member::get()
                ->filter('Username', $this->owner->Username)
                ->exclude('ID', $this->owner->ID);
        }
    }

    /**
     * @return boolean
     */
    public function sendAccountConfirmation($subject = null, $copy = null)
    {
        $subject = ($subject) ? $subject : 'Please confirm your account';

        $email = Email::create();
        $email->setTo($this->owner->Email);
        $email->setSubject($subject);
        $email->setHTMLTemplate('Email\\Member_Welcome');
        $email->setData(new ArrayData([
            'Member' => $this->owner,
            'Copy' => $copy,
            'SiteName' => Config::inst()->get('FullscreenInteractive\Source\Controllers\AppController', 'ApplicationTitle'),
            'Password' => ($this->owner->PlainPassword) ? $this->owner->PlainPassword : false
        ]));

        return $email->send();
    }

    /**
     * @param int
     *
     * @return string
     */
    public function generatePassword($length = 8)
    {
        // start with a blank password
        $password = "";
        $possible = "2346789bcdfghjkmnpqrtvwxyzBCDFGHJKLMNPQRTVWXYZ";
        $maxlength = strlen($possible);
        if ($length > $maxlength) {
            $length = $maxlength;
        }

        $i = 0;
        while ($i < $length) {
            $char = substr($possible, mt_rand(0, $maxlength-1), 1);

            if (!strstr($password, $char)) {
                $password .= $char;
                $i++;
            }
        }

        return $password;
    }

    public function hasConfirmedAccount()
    {
        $confirmed = $this->owner->dbObject('ConfirmedAccount');

        if ($confirmed->NiceAsBoolean()) {
            return true;
        }

        if (Permission::check('ADMIN')) {
            return true;
        }

        return false;
    }

    /**
     *
     */
    public function registerMobileLogin()
    {
        if ($this->owner->MobileSessionToken) {
            return;
        }

        $generator = new RandomGenerator();
        $token = $this->owner->encryptWithUserSettings($generator->randomToken()) . '-'. $this->owner->ID;

        $this->owner->MobileSessionToken = $token;

        // avoid using write to prevent recursion
        DB::query(sprintf(
            'UPDATE Member SET MobileSessionToken = \'%s\' WHERE ID = %s',
            $token,
            $this->owner->ID
        ));
    }

    /**
     * @return DataList
     */
    public function getNotifications()
    {
        return Notification::get()->where(sprintf(
            'AuthorID = %1$s OR (DeliverInsideApp = 1 AND (SELECT COUNT(*) FROM Notification_Targets WHERE MemberID = %s AND NotificationID = Notification.ID AND Deleted = 0) > 0) AND (ScheduledTime IS NULL OR ScheduledTime < NOW())',
            $this->owner->ID
        ))->sort('Created', 'DESC');
    }

    /**
     * @return DataList
     */
    public function getUnreadNotifications()
    {
        return Notification::get()->where(sprintf(
            'DeliverInsideApp = 1 AND (SELECT COUNT(*) FROM Notification_Targets WHERE MemberID = %s AND NotificationID = Notification.ID AND Deleted = 0 AND `Read` = 0) > 0 AND (ScheduledTime IS NULL OR ScheduledTime < NOW())',
            $this->owner->ID
        ))->sort('Created', 'DESC');
    }

    /**
     * @return array
     */
    public function toApi($context = [])
    {
        $data = new ArrayData([
            'id' => $this->owner->ID,
            'first_name' => (string) $this->owner->FirstName,
            'last_name' => (string) $this->owner->Surname,
            'last_synced' => time(),
            'unread_notifications' => $this->owner->getUnreadNotifications()->count(),
            'email' => (string) $this->owner->Email,
            'last_edited' => $this->owner->dbObject('LastEdited')->getTimestamp(),
            'disable_push'=> (boolean) $this->owner->DisablePushNotifications,
            'disable_email' => (boolean) $this->owner->DisableEmailNotifications,
            'gravatar' => $this->owner->Gravatar()
        ]);

        if (isset($context['auth']) && $context['auth'] === true) {
            if (!$this->owner->MobileSessionToken) {
                // generate a code
                $this->owner->registerMobileLogin();
            }

            $data->setField('session_token', $this->owner->MobileSessionToken);
        }

        $this->owner->extend('updateToApi', $data, $context);

        return $data;
    }

    /**
     * @return string
     */
    public function Gravatar($size = 40)
    {
        if ($this->owner->Photo()->exists()) {
            $fill = $this->owner->Photo()->Fill($size, $size);

            if ($fill) {
                return Controller::join_links($fill->getAbsoluteURL());
            }
        }

        return sprintf(
            'https://www.gravatar.com/avatar/%s/?s=%s&d=mm',
            md5(trim(strtolower($this->owner->Email))),
            $size
        );
    }
}
