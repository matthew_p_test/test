<?php

namespace FullscreenInteractive\Source\Extensions;

use SilverStripe\Forms\ReadonlyField;
use SilverStripe\Forms\FieldList;
use SilverStripe\Security\Permission;
use SilverStripe\ORM\DataExtension;
use SilverStripe\ORM\DB;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\Control\Email\Mailer;

class SiteConfigExtension extends DataExtension
{
    private static $db = [

    ];

    /**
     *
     */
    public function updateCMSFields(FieldList $fields)
    {
        if (Permission::check('ADMIN')) {
            $fields->addFieldsToTab('Root.System', [
                ReadonlyField::create('PHPVersion', 'PHP Version', PHP_VERSION_ID),
                ReadonlyField::create('PHPTime', 'PHP Current Time', date('r')),
                ReadonlyField::create('MySQLTime', 'MySQL Current Time', DB::query('SELECT NOW()')->value()),
                ReadonlyField::create('Mailer', 'Mailer', get_class(Injector::inst()->get(Mailer::class)))
            ]);
        }
    }
}
