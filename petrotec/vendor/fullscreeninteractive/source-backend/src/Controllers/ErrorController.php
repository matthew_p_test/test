<?php

namespace FullscreenInteractive\Source\Controllers;

use SilverStripe\Logging\DetailedErrorFormatter;
use SilverStripe\Core\Injector\Injector;
use FullscreenInteractive\Source\Controllers\AppController;
use \Exception;

class ErrorController extends AppController
{
    public function index()
    {
        $format = Injector::inst()->create(DetailedErrorFormatter::class);

        return $format->format(['context' => ['exception' => new Exception('Test Exception')]]);
    }
}
