<?php

namespace FullscreenInteractive\Source\Controllers;

use FullscreenInteractive\Source\Controllers\AppController;
use SilverStripe\View\ArrayData;
use SilverStripe\Security\Member;
use SilverStripe\Security\Security;
use SilverStripe\ORM\FieldType\DBField;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\Security\IdentityStore;
use SilverStripe\Control\Controller;

class ConfirmAccountController extends AppController
{
    private static $allowed_actions = [
        'account'
    ];

    private static $title = 'Account Confirmed';

    private static $content = '<p>Great! Thanks for confirming your account email address. You\'re all set to go by clicking the button below.</p>';

    public function account()
    {
        $id = $this->request->param('ID');
        $other = $this->request->param('OtherID');

        if (!$id || !$other) {
            return $this->httpError(400);
        }

        $member = Member::get()->filter([
            'ID' => $id
        ])->first();

        if ($member && ($member->ConfirmedAccount || $member->ConfirmToken === $other)) {
            $login = false;

            if ($member->ConfirmToken == $other) {
                $login = true;
            }

            $member->ConfirmedAccount = 1;
            $member->ConfirmToken = null;

            try {
                $member->write();

                if ($login) {
                    Injector::inst()->get(IdentityStore::class)->logIn($member);
                    Security::setCurrentUser($member);
                }

                $confirmed = true;
            } catch (Exception $e) {
                $confirmed = false;
            }
            return $this->customise(new ArrayData([
                'Title' => $this->config()->title,
                'Content' => DBField::create_field('HTMLText', $this->config()->content),
                'Confirmed' => $confirmed
            ]))->renderWith(['ConfirmAccountController', 'AppController']);
        } else {
            return $this->httpError(404, 'Could not find a member with that confirm token.');
        }
    }
}
