<?php

namespace FullscreenInteractive\Source\Controllers\Api;

use SilverStripe\Security\Member;
use SilverStripe\Security\Security;
use FullscreenInteractive\Source\Model\Notification;
use FullscreenInteractive\Source\Controllers\Api;
use SilverStripe\ORM\DB;
use SilverStripe\ORM\ArrayList;
use SilverStripe\View\ArrayData;
use FullscreenInteractive\Source\ApiHelpers;

class Notifications extends Api
{
    /**
     *
     */
    private static $allowed_actions = [
        'fetch',
        'read',
        'delete',
        'statuses'
    ];

    /**
     * POST /api/notifications/fetch
     */
    public function fetch()
    {
        $this->ensureSecure();
        $this->ensureUserSignedCall();

        $member = Security::getCurrentUser();

        if ($id = $this->request->param('ID')) {
            $notification = Notification::get()->byId($id);

            if ($notification && $notification->canView($member)) {
                return $this->success($notification);
            } else {
                return $this->httpError(404);
            }
        }

        return $this->returnPaginatedList($member->getNotifications());
    }

    /**
     * POST /api/notifications/read/1
     */
    public function read()
    {
        $this->ensureSecure();
        $this->ensureUserSignedCall();
        $this->ensureResourceID();

        $notification = Notification::get()->byId($this->request->param('ID'));

        if (!$notification || !$notification->canView()) {
            return $this->httpError(404);
        }

        $notification->markAsRead();

        return $this->success($notification);
    }

    /**
     * POST /api/notifications/delete/1
     */
    public function delete()
    {
        $this->ensureSecure();
        $this->ensureUserSignedCall();
        $this->ensureResourceID();

        $notification = Notification::get()->byId($this->request->param('ID'));

        if (!$notification || !$notification->canView()) {
            return $this->httpError(404);
        }

        $notification->markAsDeleted();

        return $this->success();
    }

    public function statuses()
    {
        $this->ensureSecure();
        $this->ensureUserSignedCall();
        $this->ensureResourceID();

        $notification = Notification::get()->byId($this->request->param('ID'));

        if (!$notification || !$notification->canEdit()) {
            return $this->httpError(404);
        }

        $list = new ArrayList();
        $targets = $notification->Targets();

        if ($targets) {
            foreach ($targets as $target) {
                if ($target->exists()) {
                    $list->push(new ArrayData([
                        'ID' => $target->ID,
                        'member' => $target->toApi(['parent' => true])->toMap(),
                        'read' => (!!$target->Read),
                        'read_date' => strtotime($target->ReadDate)
                    ]));
                }
            }
        }

        return $this->returnList($list);
    }
}
