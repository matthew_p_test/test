<?php

namespace FullscreenInteractive\Source\Controllers\Api;

use SilverStripe\Security\Member;
use SilverStripe\Security\Security;
use SilverStripe\Security\MemberAuthenticator\MemberAuthenticator;
use SilverStripe\Control\Email\Email;
use SilverStripe\Security\RandomGenerator;
use FullscreenInteractive\Source\Controllers\Api;
use FullscreenInteractive\Source\Model\DeviceToken;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\Assets\Upload;
use SilverStripe\Assets\Image;
use SilverStripe\Assets\Storage\AssetStore;
use SilverStripe\ORM\FieldType\DBDatetime;

class User extends Api
{
    private static $profile_fields_map = [
        'first_name' => 'FirstName',
        'last_name' => 'Surname',
        'password' => 'Password',
        'disable_push' => 'DisablePushNotifications',
        'disable_email' => 'DisableEmailNotifications',
        'email' => 'Email'
    ];

    /**
     *
     */
    private static $allowed_actions = [
        'login',
        'register',
        'forgot',
        'fetch',
        'token',
        'profile',
        'changepassword',
        'savepicture'
    ];

    /**
     * POST /api/user/login
     *
     * Logs a user in by the given email, password and apiToken.
     */
    public function login()
    {
        $this->ensureSecure();

        $username = $this->getVar('username');
        $password = $this->getVar('password');

        if (!$username || !$password) {
            return $this->httpError(400, 'Missing username or password field.');
        }

        $auth = Injector::inst()->create(MemberAuthenticator::class);

        $check = $auth->authenticate([
            'Email' => $username,
            'Password' => $password
        ], $this->request);

        if (!$check) {
            $member = null;

            if (strpos($username, '@') !== false) {
                $member = Member::get()->filter('Email', $username)->first();
            } else {
                $member = Member::get()->filter('Username', $username)->first();
            }

            if ($member) {
                // either we auth on the email or username
                if (Member::config()->unique_identifier_field == 'Username') {
                    $field = $member->Username;
                } else {
                    $field = $member->Email;
                }

                $check = $auth->authenticate([
                    'Email' => $field,
                    'Password' => $password
                ], $this->request);
            }
        }

        if ($check) {
            $check->registerMobileLogin();

            return json_encode($check->toApi(['auth' => true])->toMap());
        } else {
            return $this->httpError(400, 'Invalid login details provided. Please check your username and password');
        }
    }

    /**
     * POST /api/user/register
     *
     * Creates a user
     */
    public function register()
    {
        $this->ensureSecure();

        $fields = [
            'first_name',
            'last_name',
            'email'
        ];

        foreach ($fields as $field) {
            if (!$this->getVar($field)) {
                return $this->httpError(400, 'Missing field '. $field);
            }
        }

        $member = Member::get()->filter('Email', $this->getVar('email'))->first();

        if ($member) {
            return $this->httpError(400, 'Sorry you cannot register that email as another member is already using it.');
        }

        $member = Member::create();

        if (!$password = $this->getVar('password')) {
            $password = $member->generatePassword();
        }

        if (!$username = $this->getVar('username')) {
            $username = $this->getVar('email');
        }

        $member->FirstName = $this->getVar('first_name');
        $member->Surname = $this->getVar('last_name');
        $member->Email = $this->getVar('email');
        $member->Username = $username;
        $member->write();

        $validationResult = $member->changePassword($password);

        if (!$validationResult->isValid()) {
            return $this->httpError(400, 'Invalid password');
        }

        $member->PlainPassword = $password;
        $member->sendAccountConfirmation();

        return json_encode($member->toApi(['auth' => true])->toMap());
    }

    /**
     * GET /api/user/fetch
     *
     * If an ID is passed then it retrieves information about that given user.
     */
    public function fetch()
    {
        $this->ensureSecure();
        $this->ensureUserSignedCall();

        $member = Member::get()->byId($this->request->param('ID'));

        if (!$member) {
            return $this->httpError(404, 'Cannot find member with that ID');
        }

        if (!$member->canView()) {
            return $this->httpError(403);
        }

        if (Security::getCurrentUser()->ID === $member->ID) {
            return json_encode($member->toApi(['auth' => true])->toMap());
        }

        return json_encode($member->toApi()->toMap());
    }

    /**
     * POST /api/user/forgot
     */
    public function forgot()
    {
        $this->ensureSecure();

        $key = $this->getVar('emailOrUserName');

        $member = Member::get()->filter("Email", $key)->first();

        if (!$member) {
            $member = Member::get()->filter("Username", $key)->first();
        }

        if ($member && $member->Email) {
            $token = $member->generateAutologinTokenAndStoreHash();

            Email::create()
                ->setHTMLTemplate('SilverStripe\\Control\\Email\\ForgotPasswordEmail')
                ->setData($member)
                ->setSubject(_t('SilverStripe\\Security\\Member.SUBJECTPASSWORDRESET', "Your password reset link", 'Email subject'))
                ->addData('PasswordResetLink', Security::getPasswordResetLink($member, $token))
                ->setTo($member->Email)
                ->send();
        }

        return $this->success();
    }

    /**
     * POST /api/user/token
     */
    public function token()
    {
        $this->ensureSecure();
        $this->ensureUserSignedCall();

        $token = $this->getVar('deviceToken');
        $os = $this->getVar('os');

        $member = Security::getCurrentUser();

        if ($member && $token) {
            $dt = DeviceToken::get()->filter([
                'Token' => $token
            ])->first();

            if (!$dt || !$dt->exists()) {
                $dt = DeviceToken::create();
                $dt->Token = $token;
                $dt->Platform = $os;
            }

            $dt->MemberID = $member->ID;
            $dt->write();

            return $this->success();
        } else {
            return $this->httpError(400, 'Invalid deviceToken input');
        }
    }

    /**
     * POST /api/user/profile
     *
     * Updates a user profile field
     */
    public function profileField()
    {
        $this->ensureSecure();
        $this->ensureUserSignedCall();

        $member = Security::getCurrentUser();

        $field = $this->getField('field');
        $value = $this->getField('value');
        $fields = $this->config()->profile_fields_map;

        if (isset($fields[$field]) && $value !== null) {
            $key = $fields[$field];
            $member->$key = $value;
        }

        $member->write();

        return json_encode($member->toApi(['auth' => true])->toMap());
    }

    public function profile()
    {
        $this->ensureSecure();
        $this->ensureUserSignedCall();

        $member = Security::getCurrentUser();

        $fields = $this->config()->profile_fields_map;

        foreach ($fields as $field => $key) {
            $value = $this->getVar($field);
            if ($value !== null) {
                $member->{$key} = $value;
            }
        }

        $member->write();

        return json_encode($member->toApi(['auth' => true])->toMap());
    }

    public function changepassword()
    {
        $this->ensureSecure();
        $this->ensureUserSignedCall();

        $password = $this->getVar('password');
        $newPassword = $this->getVar('newPassword');

        if (!$password || !$newPassword) {
            return $this->httpError(400, 'Missing Password');
        }

        $member = Security::getCurrentUser();
        $auth = Injector::inst()->create(MemberAuthenticator::class);

        try {
            $validationResult = $member->changePassword($newPassword);

            if (!$validationResult->isValid()) {
                return $this->httpError(400, 'Invalid password change');
            }

            // Clear locked out status
            $member->LockedOutUntil = null;
            $member->FailedLoginCount = null;

            // Clear the members login hashes
            $member->AutoLoginHash = null;
            $member->AutoLoginExpired = DBDatetime::create()->now();
            $member->write();

            return json_encode($member->toApi()->toMap());
        } catch (\Exception $e) {
            return $this->httpError(500, $e->getMessage());
        }
    }

    public function savepicture()
    {
        $this->ensureSecure();
        $this->ensureUserSignedCall();

        $imageData = $this->getVar('data');

        if (!$imageData) {
            return $this->httpError('no data provided');
        }

        // open the output file for writing
        $image = Injector::inst()->create(Image::class);
        $member = Security::getCurrentUser();

        list($type, $imageData) = explode(';', $imageData);
        list(, $extension) = explode('/', $type);
        list(, $imageData)      = explode(',', $imageData);
        $fileName = uniqid().'.'.$extension;
        $imageData = base64_decode($imageData);

        $path = TEMP_FOLDER .'/'. $fileName;
        file_put_contents($path, $imageData);

        $tmp = [
            'name' => $fileName,
            'type' => 'image/'. $type,
            'tmp_name' => $path,
            'error' => 0,
            'size' => filesize($path)
        ];

        $image = new Image();
        $image->setFromLocalFile($path, $fileName, AssetStore::CONFLICT_OVERWRITE);
        $image->write();

        $member->PhotoID = $image->ID;
        $member->write();

        return json_encode($member->toApi()->toMap());
    }
}
