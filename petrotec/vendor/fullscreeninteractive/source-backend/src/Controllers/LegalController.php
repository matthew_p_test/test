<?php

namespace FullscreenInteractive\Source\Controllers;

use FullscreenInteractive\Source\Controllers\AppController;
use SilverStripe\View\ArrayData;

class LegalController extends AppController
{
    private static $allowed_actions = [
        'terms_of_service',
        'privacy_policy'
    ];

    public function terms_of_service()
    {
        return $this->customise(new ArrayData([
            'Title' => 'Terms of Service'
        ]))->renderWith(['Terms', 'AppController']);
    }

    public function privacy_policy()
    {
        return $this->customise(new ArrayData([
            'Title' => 'Privacy Policy'
        ]))->renderWith(['Privacy', 'AppController']);
    }
}
