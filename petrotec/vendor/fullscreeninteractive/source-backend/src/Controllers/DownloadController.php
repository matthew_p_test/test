<?php

namespace FullscreenInteractive\Source\Controllers;

use SilverStripe\View\ArrayData;

/**
 * Handles auto redirecting a user to the correct store to download the app if
 * possible. Otherwise just displays a page with the 2 download options.
 */
class DownloadController extends AppController
{
    protected $segment = 'download';

    private static $apple_download_url = '';

    private static $android_download_url = '';

    private static $supports_ios = true;

    private static $supports_android = true;

    private static $allowed_actions = [
        'index',
        'apple',
        'android'
    ];

    public function index()
    {
        if (!isset($_SERVER['HTTP_USER_AGENT'])) {
            return $this->customise(new ArrayData([
                'Title' => 'Download App',
                'Apple' => $this->config()->apple_download_url,
                'Android' => $this->config()->android_download_url
            ]))->renderWith(['DownloadController', 'AppController']);
        }

        $iPod    = stripos($_SERVER['HTTP_USER_AGENT'], "iPod");
        $iPhone  = stripos($_SERVER['HTTP_USER_AGENT'], "iPhone");
        $iPad    = stripos($_SERVER['HTTP_USER_AGENT'], "iPad");
        $android = stripos($_SERVER['HTTP_USER_AGENT'], "Android");

        if ($iPod || $iPhone || $iPad) {
            return $this->apple();
        } elseif ($android) {
            return $this->android();
        } else {
            return $this->customise(new ArrayData([
                'Title' => 'Download App',
                'Apple' => $this->config()->apple_download_url,
                'Android' => $this->config()->android_download_url
            ]))->renderWith(['DownloadController', 'AppController']);
        }
    }

    public function apple()
    {
        return $this->redirect($this->config()->apple_download_url);
    }

    public function android()
    {
        return $this->redirect($this->config()->android_download_url);
    }

    public function canView()
    {
        return true;
    }
}
