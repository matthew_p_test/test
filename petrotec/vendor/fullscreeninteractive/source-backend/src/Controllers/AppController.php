<?php

namespace FullscreenInteractive\Source\Controllers;

use SilverStripe\Control\Controller;
use SilverStripe\View\Requirements;
use SilverStripe\Control\Director;
use SilverStripe\Control\Session;
use SilverStripe\View\ArrayData;
use SilverStripe\Security\MemberAuthenticator\MemberLoginForm;
use SilverStripe\Security\Member;
use SilverStripe\ORM\FieldType\DBField;
use SilverStripe\Control\HTTPResponse;
use SilverStripe\Control\HTTPResponse_Exception;
use SilverStripe\Security\MemberAuthenticator;
use SilverStripe\Core\Config\Config;
use SilverStripe\Security\Security;
use SilverStripe\ORM\DB;
use Exception;

class AppController extends Controller
{
    protected $segment = '';

    private static $time_zone;

    private static $allowed_actions = [
        'index',
        'LoginForm'
    ];

    private static $casting = [
        'StatusMessage' => 'HTMLText'
    ];

    /**
     * Handle 404 errors gracefully as the normal 404 error pages are part
     * of the CMS module
     */
    public function handleAction($request, $action)
    {
        if ($time = self::config()->get('time_zone')) {
            try {
                DB::query(sprintf("SET time_zone = '%s'", $time));
            } catch (Exception $e) {
                //
            }
        }

        try {
            $response = parent::handleAction($request, $action);

            return $response;
        } catch (HTTPResponse_Exception $e) {
            $response = $e->getResponse();
            $response->addHeader('Content-Type', 'text/html; charset=utf-8');
            $response->setBody($this->renderWith(array('Error', 'AppController')));

            return $response;
        }
    }

    /**
     * Send JSON
     */
    public function sendJson(array $data)
    {
        $this->response->addHeader('Content-Type', 'application/json; charset=utf-8');
        $this->response->setBody(json_encode($data));
        
        return $this->response;
    }

    /**
     * Return a HTTP error to the user
     */
    public function httpError($errorCode = 404, $errorMessage = null)
    {
        $template = array('Error', 'AppController');

        if (!$errorMessage) {
            switch ($errorCode) {
                case 403:
                    $errorMessage = 'You do not have permission to view this information.';
                    break;
                case 404:
                    $errorMessage = 'That information no longer exists or cannot be found.';
                    break;
                default:
                    $errorMessage = 'Invalid request to the server. Please try another URL.';
            }
        }

        $result = new ArrayData([
            'Title' => 'Whoops!',
            'Content' => DBField::create_field('HTMLText', $errorMessage),
        ]);

        return parent::httpError(
            $errorCode,
            new HTTPResponse(
                $this->customise($result)->renderWith($template)
            )
        );
    }

    public function init()
    {
        parent::init();

        Requirements::block(THIRDPARTY_DIR . '/jquery/jquery.js');

        $session = $this->getRequest()->getSession();

        if ($message = $session->get('Status')) {
            $session->clear('Status');

            $this->StatusMessage = $message['Message'];
            $this->StatusType = (isset($message['Type'])) ? $message['Type'] : 'success';
        }
    }

    public function setAppStatusMessage($message, $type = 'success')
    {
        $session = $this->getRequest()->getSession();

        $session->set(
            'Status',
            [
            'Message' => $message,
            'Type' => $type
            ]
        );

        $this->StatusMessage = $message;
        $this->StatusType = $type;
    }

    public function Link($action = null)
    {
        return Controller::join_links(
            Director::baseURL(),
            $this->segment,
            $action
        );
    }

    public function LoginForm()
    {
        $form = MemberLoginForm::create($this, MemberAuthenticator::class, 'LoginForm');
        $form->setFormAction('Security/login/default/LoginForm');
        return $form;
    }

    public function index()
    {
        if (Member::currentUser()) {
            return $this->redirect(Config::inst()->get(Security::class, 'default_login_dest'));
        }

        return $this->customise(new ArrayData([
            'Title' => $this->config()->title,
            'Form' => $this->LoginForm(),
        ]))->renderWith(['Security', 'AppController']);
    }

    public function ApplicationTitle()
    {
        return $this->config()->ApplicationTitle;
    }

    public function ApplicationOrg()
    {
        return $this->config()->ApplicationOrg;
    }


    public function ClassName()
    {
        return get_class($this);
    }

    /**
     * Adds a helper class to the body
     *
     * @return string
     */
    public function BodyClass()
    {
        if (strpos(get_class($this), '\\') !== false) {
            $last = array_slice(explode('\\', get_class($this)), -1)[0];

            return strtolower($last);
        }

        return strtolower(get_class($this));
    }

    public static function forceNonSSL($patterns = null, $secureDomain = null)
    {
        if (!isset($_SERVER['REQUEST_URI'])) {
            return false;
        }

        $matched = false;

        if ($patterns) {
            // Calling from the command-line?
            if (!isset($_SERVER['REQUEST_URI'])) {
                return;
            }

            $relativeURL = Director::makeRelative(
                Director::absoluteURL($_SERVER['REQUEST_URI'])
            );

            // protect portions of the site based on the pattern
            foreach ($patterns as $pattern) {
                if (preg_match($pattern, $relativeURL)) {
                    $matched = true;
                    break;
                }
            }
        } else {
            // protect the entire site
            $matched = true;
        }

        if ($matched && Director::is_https()) {
            // if an domain is specified, redirect to that instead of the current domain
            if ($secureDomain) {
                $url = 'http://' . $secureDomain . $_SERVER['REQUEST_URI'];
            } else {
                $url = $_SERVER['REQUEST_URI'];
            }

            $destURL = str_replace('https:', 'http:', Director::absoluteURL($url));

            // This coupling to SapphireTest is necessary to test the destination URL and to not interfere with tests
            if (class_exists(SapphireTest::class, false) && SapphireTest::is_running_test()) {
                return $destURL;
            } else {
                echo "<html><head><meta http-equiv='refresh' content=\"0;URL='$destURL'\"></head></html>";
                die();
            }
        } else {
            return false;
        }
    }

    public static function RandomQuery($fake = null)
    {
        return time() . rand(0, 10);
    }

    public static function get_template_global_variables()
    {
        return [
            'RandomQuery' => 'RandomQuery'
        ];
    }
}
