<?php

namespace FullscreenInteractive\Source\Controllers;

use SilverStripe\Control\Controller;
use SilverStripe\Security\Member;
use SilverStripe\Security\Security;
use SilverStripe\Control\HTTPResponse;
use SilverStripe\Control\HTTPResponse_Exception;
use SilverStripe\ORM\PaginatedList;
use SilverStripe\ORM\SS_List;
use FullscreenInteractive\Source\ApiReadable;
use FullscreenInteractive\Source\ApiHelpers;
use FullscreenInteractive\Source\Sentry;
use SilverStripe\View\ArrayData;

class Api extends Controller
{
    /**
     * @var array
     */
    private static $allowed_actions = [

    ];

    /**
     *
     */
    private static $page_length = 10;

    /**
     * @var array
     */
    protected $vars;

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->vars = $this->request->requestVars();

        if (strpos($this->request->getHeader('Content-Type'), 'application/json') !== false) {
            $postVars = json_decode(file_get_contents("php://input"), true);

            if ($postVars) {
                $this->vars = array_merge($this->vars, $postVars);
            }
        }

        if ($this->vars) {
            $this->vars = array_change_key_case($this->vars);
        }

        $this->response->addHeader('Access-Control-Allow-Origin', '*');
        $this->response->addHeader("Content-type", "application/json");
    }

    public function index()
    {
        return $this->success();
    }

    /**
     *
     */
    public function handleAction($request, $action)
    {
        try {
            $response = parent::handleAction($request, $action);

            if ($response instanceof HTTPResponse_Exception) {
                return $response->getResponse();
            }

            return $response;
        } catch (HTTPResponse_Exception $e) {
            $response = $e->getResponse();
            $response->setStatusCode(500);
            $response->addHeader('Access-Control-Allow-Origin', '*');
            $response->addHeader("Content-type", "application/json");

            return $response;
        } catch (\Exception $e) {
            $response = new HTTPResponse();
            $response->setBody(
                json_encode(
                    [
                    'success' => false,
                    'error' => $e->getMessage(),
                    'code' => 500
                    ]
                )
            );

            $response->setStatusCode(500);
            $response->addHeader('Access-Control-Allow-Origin', '*');
            $response->addHeader("Content-type", "application/json");

            return $response;
        }
    }

    /**
     * @param string
     */
    public function getVar($name)
    {
        $key = strtolower($name);

        return (isset($this->vars[$key])) ? $this->vars[$key] : null;
    }

    /**
     * Performs the authentication to validate that we can make calls on this
     * API. The Mobile API token should be defined in the environment file.
     */
    public function ensureSecure()
    {
        $token = $this->request->getHeader('Secure-Token');

        if (!$token) {
            $exec = $this->httpError(403, 'Missing API Token');
            Sentry::logException($exec);

            throw $exec;
        }

        if ($token !== MOBILE_APP_TOKEN) {
            $exec = $this->httpError(403, 'API Token invalid');
            Sentry::logException($exec);

            throw $exec;
        }
    }

    /**
     * In addition to the mobile api token, if a call needs special permissions
     * then it should also be provided with a header for the api session. This
     * relates to a individual member and allows calls to be permission based.
     */
    public function ensureUserSignedCall()
    {
        $token = $this->request->getHeader('Secure-Token-Session');

        if (!$token) {
            $exec = $this->httpError(403, 'Missing API Session Token. Please Login');
            Sentry::logException($exec);

            throw $exec;
        }

        // ensure that this secure token session matches a given user.
        $parts = explode('-', $token);

        if (count($parts) !== 2) {
            $exec = $this->httpError(400, 'Invalid Secure-Token-Session');
            Sentry::logException($exec);

            throw $exec;
        }

        $member = Member::get()->byId($parts[1]);

        if (!$member) {
            $exec = $this->httpError(403, sprintf('Unknown or expired Secure-Token-Session (value: %s)', $token));
            Sentry::logException($exec);

            throw $exec;
        }

        if ($member->MobileSessionToken !== $token) {
            $exec = $this->httpError(403, sprintf('Invalid Secure-Token-Session (value: %s, member: %s)', $token, $parts[1]));
            Sentry::logException($exec);

            throw $exec;
        }

        Security::setCurrentUser($member);
    }

    /**
     *
     */
    public function ensureResourceID()
    {
        $id = $this->request->param('ID');

        if (!$id) {
            $exec = $this->httpError(400, 'No resource ID provided');

            throw $exec;
        }
    }

    /**
     *
     */
    public function httpError($errorCode, $errorMessage = null)
    {
        if (!$errorMessage) {
            switch ($errorCode) {
                case 404:
                    $errorMessage = 'Missing resource';
                    break;

                default:
                    $errorMessage = 'Permission denied resource';
                    break;
            }
        }

        $body = json_encode([
            'error' => $errorMessage,
            'code' => $errorCode,
            'controller' => get_class($this)
        ]);

        $response = new HTTPResponse(
            $body,
            $errorCode
        );

        $response->addHeader("Content-type", "application/json");

        $err = new HTTPResponse_Exception();
        $err->setResponse($response);

        return $err;
    }

    /**
     * Helper for a generic success message or an object if provided
     *
     * @param ApiReadable|array $obj
     */
    public function success($obj = null)
    {
        if ($obj) {
            if (is_array($obj)) {
                return json_encode($obj);
            } else {
                return json_encode($obj->toApi()->toMap());
            }
        }

        return $this->sendJson(json_encode([
            'success' => true,
            'timestamp' => time()
        ]));
    }

    public function sendJson($json)
    {
        if (is_array($json)) {
            $json = json_encode($json);
        } elseif ($json instanceof ArrayData) {
            $json = json_encode($json->toMap());
        }

        $this->response->addHeader("Content-type", "application/json");
        $this->response->setBody($json);

        return $this->response;
    }


    /**
     *
     */
    public function returnPaginatedList(SS_List $list, $keyFunc = null, $dataFunc = null)
    {
        $list = new PaginatedList($list, $this->request);
        $limit = $this->config()->page_length;

        if ($limit > 100) {
            $limit = 100;
        }

        $list->setPageLength($limit);
        $output = [];

        if (!$keyFunc) {
            $keyFunc = function ($item) {
                return $item->ID;
            };
        }

        foreach ($list as $item) {
            if ($dataFunc) {
                $output[$keyFunc($item)] = $dataFunc($item);
            } else {
                $output[$keyFunc($item)] = ApiHelpers::getApiDataFromObj($item);
            }
        }

        return json_encode([
            'records' => $output,
            'start' => $list->getPageStart(),
            'limit' => $list->getPageLength(),
            'total' => $list->getTotalItems(),
            'more' => ($list->NextLink()) ? true : false,
            'hasMore' => ($list->NextLink()) ? true : false // legacy
        ]);
    }

    /**
     * Returns a list of records in the API in the same structure as a paginated
     * list, apart from this happens to never have any more
     *
     *
     */
    public function returnList(SS_List $list, $keyFunc = null, $encode = true)
    {
        $output = [];

        if (!$keyFunc) {
            $keyFunc = function ($item) {
                return $item->ID;
            };
        }

        foreach ($list as $item) {
            $output[$keyFunc($item)] = ApiHelpers::getApiDataFromObj($item);
        }

        $data = [
            'records' => $output,
            'start' => 0,
            'limit' => null,
            'total' => $list->count(),
            'more' => false
        ];

        return ($encode) ? json_encode($data) : $data;
    }

    /**
     * Helper to check that certain POST or GET vars always exist.
     *
     * @param $vars array
     */
    public function ensureVars($vars)
    {
        foreach ($vars as $var) {
            if ($this->getVar($var) === null) {
                $exec = $this->httpError(400, "Missing required field $var");

                throw $exec;
            }
        }
    }
}
