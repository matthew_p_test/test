<?php

namespace FullscreenInteractive\Source\Controllers;

use FullscreenInteractive\Source\Forms\RegisterForm;
use FullscreenInteractive\Source\Controllers\AppController;

use SilverStripe\View\ArrayData;
use SilverStripe\Security\Member;
use SilverStripe\Control\Controller;
use SilverStripe\Control\Director;

class RegisterController extends AppController
{
    private static $allowed_actions = [
        'index',
        'thanks',
        'RegisterForm'
    ];

    private static $register_title = 'Register';

    private static $thanks_title = 'Thanks for registering';

    public function RegisterForm()
    {
        return new RegisterForm($this, __FUNCTION__);
    }

    public function Link($action = null)
    {
        return Controller::join_links('register', $action);
    }

    public function thanks()
    {
        return $this->customise(new ArrayData([
            'Title' => $this->config()->thanks_title,
            'ShowThanks' => true,
            'Form' => null
        ]))->renderWith(['AppController_register', 'AppController']);
    }

    public function index()
    {
        return $this->customise(new ArrayData([
            'Title' => $this->config()->register_title,
            'Form' => $this->RegisterForm(),
            'ShowThanks' => false
        ]))->renderWith(['AppController_register', 'AppController']);
    }
}
