<?php

namespace FullscreenInteractive\Source\Controllers;

use SilverStripe\Security\Security;

class SecureController extends AppController
{
    public function init()
    {
        parent::init();

        if (!Security::getCurrentUser()) {
            return Security::permissionFailure();
        }
    }
}
