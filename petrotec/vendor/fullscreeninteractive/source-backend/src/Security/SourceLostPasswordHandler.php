<?php

namespace FullscreenInteractive\Source\Security;

use SilverStripe\Security\MemberAuthenticator\LostPasswordHandler;
use SilverStripe\Security\Member;

class SourceLostPasswordHandler extends LostPasswordHandler
{
    /**
     * Load an existing Member from the provided data.
     *
     * @param  array $data
     * @return Member|null
     */
    protected function getMemberFromData(array $data)
    {
        if (!empty($data['Email'])) {
            return Member::get()->filter([
                'Email' => $data['Email'
            ]])->first();
        }
    }
}
