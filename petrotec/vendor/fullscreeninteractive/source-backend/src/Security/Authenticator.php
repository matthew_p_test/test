<?php

namespace FullscreenInteractive\Source\Security;

use SilverStripe\Security\MemberAuthenticator\MemberAuthenticator;
use SilverStripe\Security\Member;
use SilverStripe\ORM\ValidationResult;
use SilverStripe\Security\Security;

class Authenticator extends MemberAuthenticator
{
    protected $foundMember = false;

    protected function authenticateMember($data, ValidationResult &$result = null, Member $member = null)
    {
        $success = parent::authenticateMember($data, $result, $member);

        if ($success === null && !$this->foundMember && Member::config()->get('unique_identifier_field') !== 'Email') {
            $email = !empty($data['Email']) ? $data['Email'] : null;

            $candidates = Member::get()
                ->filter(['Email' => $email]);

            if ($candidates) {
                foreach ($candidates as $candidate) {
                    $result = ValidationResult::create();

                    $success = parent::authenticateMember($data, $result, $candidate);

                    if ($success !== null && $result->isValid()) {
                        return $success;
                    }
                }
            }
        }

        return $success;
    }

    public function checkPassword(Member $member, $password, ValidationResult &$result = null)
    {
        if ($member && !$member->PasswordEncryption) {
            $member->PasswordEncryption = Security::config()->get('password_encryption_algorithm');
            $member->write();
        }

        $this->foundMember = true;

        return parent::checkPassword($member, $password, $result);
    }
}
