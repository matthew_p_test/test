<?php

namespace FullscreenInteractive\Source\Push;

use FullscreenInteractive\Source\Model\DeviceToken;
use Sly\NotificationPusher\PushManager;
use SilverStripe\Core\Config\Config;
use Sly\NotificationPusher\Adapter\Gcm as GcmAdapter;
use Sly\NotificationPusher\Adapter\Apns as ApnsAdapter;
use Sly\NotificationPusher\Collection\DeviceCollection;
use Sly\NotificationPusher\Model\Device;
use Sly\NotificationPusher\Model\Message;
use Sly\NotificationPusher\Model\Push;
use SilverStripe\Core\Config\Configurable;

class AirMessage
{
    use Configurable;

    protected $notification;

    protected $job;

    protected $verbose = false;

    private static $env = 'prod';

    public function __construct($notification, $job = null)
    {
        $this->notification = $notification;

        if ($job) {
            $this->job = $job;
        } else {
            $this->job = new AirMessage_Job();
        }
    }

    public function deliver()
    {
        $tokens = $this->notification->getActiveDeviceTokens();
        $env = $this->config()->env;

        if (!$env) {
            $env = PushManager::ENVIRONMENT_PROD;
        }

        if (!$tokens) {
            return false;
        }

        $ios = [];
        $gcm = [];

        foreach ($tokens as $token) {
            if ($token->Platform === 'iOS') {
                $ios[] = new Device($token->Token);
            } else {
                $gcm[] = new Device($token->Token);
            }
        }

        if (count($ios) > 0) {
            try {
                $this->deliverIos($ios, $env);
            } catch (\Exception $e) {
                $this->job->addMessage('[AirMessage iOS Exception] '. $e->getMessage());
            }
        }

        if (count($gcm) > 0) {
            try {
                $this->deliverAndroid($gcm, $env);
            } catch (\Exception $e) {
                $this->job->addMessage('[AirMessage Android Exception] '. $e->getMessage());
            }
        }

        return true;
    }

    public function wrapDeviceToken(DeviceToken $dt)
    {
        return new Device($dt->Token);
    }

    public function deliverIos($tokens, $env)
    {
        $pushManager = new PushManager($env);
        $adapter = new ApnsAdapter([
            'certificate' => BASE_PATH . '/.certs/push.pem',
        ]);

        $devices = new DeviceCollection($tokens);
        $custom = ($this->notification->CustomData) ? unserialize($this->notification->CustomData) : false;
        $payload = [
            'badge' => 1,
            'sound' => 'default',
            'custom' => ($custom) ? $custom : [],
            'content-available' => 1,
            'category' => ($custom && isset($custom['category'])) ? $custom['category'] : null
        ];

        $message = new Message($this->notification->PushMessage, $payload);

        $push = new Push($adapter, $devices, $message);

        $pushManager->add($push);
        $pushManager->push();

        foreach ($push->getResponses() as $token => $response) {
            $this->job->addMessage(sprintf(
                '[iOS/%s] deviceId: %s, hasCert: %s response: %s',
                $env,
                ($token) ? $token : 'NO!!',
                (file_exists(BASE_PATH . '/.certs/push.pem')) ? 'yes ('. BASE_PATH . '/.certs/push.pem)': 'NO!!',
                print_r($response, true)
            ));
        }
    }

    public function deliverAndroid($tokens, $env)
    {
        $pushManager = new PushManager($env);
        $adapter = new GcmAdapter([
            'apiKey' => Config::inst()->get(AirMessage::class, 'fcm_api_key')
        ]);

        $devices = new DeviceCollection($tokens);

        $message = new Message($this->notification->PushMessage, [
            'icon' => 'new_message',
            'sound' => 'default',
            'body' => $this->notification->PushMessage,
            'title' => $this->notification->PushTitle,
            'notificationData' => ($data = $this->notification->CustomData) ? unserialize($data) : []
        ]);

        $push = new Push($adapter, $devices, $message);

        $pushManager->add($push);
        $pushManager->push();

        foreach ($push->getResponses() as $token => $response) {
            $this->job->addMessage(sprintf(
                '[FCM/%s] deviceId: %s, apiKey: %s response: %s',
                $env,
                ($token) ? $token : 'NO!!',
                (Config::inst()->get(AirMessage::class, 'fcm_api_key')) ? Config::inst()->get(AirMessage::class, 'fcm_api_key') : 'NO!!',
                print_r($response, true)
            ));
        }
    }
}
