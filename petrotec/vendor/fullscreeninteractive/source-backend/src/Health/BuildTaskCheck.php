<?php

namespace FullscreenInteractive\Source\Health;

use SilverStripe\EnvironmentCheck\EnvironmentCheck;
use SilverStripe\Core\ClassInfo;
use SilverStripe\Core\Injector\Injector;
use FullscreenInteractive\Source\Tasks\SourceTask;

/**
 * This check confirms that any cron jobs configured for the project are running
 * as they should be.
 *
 */
class BuildTaskCheck implements EnvironmentCheck
{
    /**
     * {@inheritDoc}
     *
     * @return array
     */
    public function check()
    {
        // retrieve all the subclasses on the tasks that have
        $tasksToCheck = ClassInfo::subclassesFor(SourceTask::class);

        $timedTasks = [];

        foreach ($tasksToCheck as $task) {
            $inst = Injector::inst()->create($task);

            if ($time = $inst->config()->get('max_expected_age')) {
                $timedTasks[$task] = $inst;
            }
        }

        if ($timedTasks) {
            foreach ($timedTasks as $task => $inst) {
                if (!$this->checkTask($inst)) {
                    $lastRan = $this->getLockFileTime($inst);

                    return [
                        EnvironmentCheck::ERROR, sprintf(
                            'Task "%s" has not run within the expected time. Last ran: %s',
                            $inst->getTitle(),
                            ($lastRan) ? date('d-m-Y H:i:sa', $lastRan) : 'never'
                        )
                    ];
                }
            }

            return [
                EnvironmentCheck::OK, sprintf('%s tasks successfully running', count($timedTasks))
            ];
        } else {
            return [
                EnvironmentCheck::OK, 'No Scheduled Cronjobs'
            ];
        }
    }

    public function checkTask(SourceTask $task)
    {
        $lastRanTime = $this->getLockFileTime($task);

        if ($lastRanTime) {
            $expected = $task->config()->get('max_expected_age');

            return ($lastRanTime > (time() - $expected));
        } else {
            return false;
        }
    }

    public function getLockFileTime(SourceTask $task)
    {
        $path = $task->getLockFilePath();

        if (file_exists($path)) {
            return filemtime($path);
        } else {
            return false;
        }
    }
}
