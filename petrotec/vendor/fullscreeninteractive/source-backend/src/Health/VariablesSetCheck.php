<?php

namespace FullscreenInteractive\Source\Health;

use SilverStripe\EnvironmentCheck\EnvironmentCheck;
use SilverStripe\Core\ClassInfo;
use SilverStripe\Core\Injector\Injector;
use FullscreenInteractive\Source\Tasks\SourceTask;
use FullscreenInteractive\Source\Push\AirMessage;
use FullscreenInteractive\Source\Controllers\AppController;
use SilverStripe\Core\Config\Config;

/**
 * This check confirms that all the correct parameters are set
 *
 */
class VariablesSetCheck implements EnvironmentCheck
{
    /**
     * {@inheritDoc}
     *
     * @return array
     */
    public function check()
    {
        // retrieve all the subclasses on the tasks that have
        $checks = [
            'fcm_api_key' => Config::inst()->get(AirMessage::class, 'fcm_api_key'),
            'ApplicationTitle' => Config::inst()->get(AppController::class, 'ApplicationTitle'),
            'ApplicationOrg' => Config::inst()->get(AppController::class, 'ApplicationTitle')
        ];

        $valid = true;

        foreach ($checks as $variable => $check) {
            if ($check == null) {
                return [
                    EnvironmentCheck::ERROR, sprintf(
                        'Variable %s is not set.',
                        $variable
                    )
                ];
            }
        }

        return [
            EnvironmentCheck::OK, sprintf(
                'Variables are all set.',
                $variable
            )
        ];
    }
}
