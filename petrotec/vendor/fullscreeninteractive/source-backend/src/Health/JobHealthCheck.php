<?php

namespace FullscreenInteractive\Source\Health;

use SilverStripe\EnvironmentCheck\EnvironmentCheck;
use Symbiote\QueuedJobs\DataObjects\QueuedJobDescriptor;

/**
 * This check looks at the queued job module and sees to make sure that jobs are
 * running and no paused or failed jobs in the last 72 hours.
 */
class JobHealthCheck implements EnvironmentCheck
{
    /**
     * {@inheritDoc}
     *
     * @return array
     */
    public function check()
    {
        $stalledOrFailedJobs = QueuedJobDescriptor::get()->where(
            "JobStatus IN ('Paused', 'New', 'Initialising', 'Broken') AND LastEdited > (NOW() - INTERVAL 7 DAY) AND LastEdited < (NOW() - INTERVAL 30 MINUTE)"
        );

        if ($stalledOrFailedJobs->count() > 0) {
            return [
                EnvironmentCheck::ERROR, sprintf(
                    'There are %s broken jobs in the queue',
                    $stalledOrFailedJobs->count()
                )
            ];
        } else {
            return [
                EnvironmentCheck::OK, 'There are no broken jobs'
            ];
        }
    }
}
