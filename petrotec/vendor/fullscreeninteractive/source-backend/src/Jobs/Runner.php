<?php

namespace FullscreenInteractive\Source\Jobs;

use Symbiote\QueuedJobs\Tasks\Engines\DoormanRunner;
use SilverStripe\ORM\FieldType\DBDatetime;
use Symbiote\QueuedJobs\DataObjects\QueuedJobDescriptor;
use Symbiote\QueuedJobs\Jobs\DoormanQueuedJobTask;
use Symbiote\QueuedJobs\Services\QueuedJob;
use AsyncPHP\Doorman\Manager\ProcessManager;
use SilverStripe\Core\Environment;
use SilverStripe\Core\Injector\Injector;
use Symbiote\QueuedJobs\Services\QueuedJobService;

/**
 * Runs all jobs through the doorman engine
 */
class Runner extends DoormanRunner
{
    /**
     * Return the fixed CLI path
     */
    public function getCliScriptPath()
    {
        return dirname(dirname(dirname(__FILE__))) ."/cli-script.php";
    }

    /**
     * Run tasks on the given queue
     *
     * @param string $queue
     */
    public function runQueue($queue)
    {
        // check if queue can be processed
        $service = QueuedJobService::singleton();
        if ($service->isAtMaxJobs()) {
            $service->getLogger()->info('Not processing queue as jobs are at max initialisation limit.');
            return;
        }

        // split jobs out into multiple tasks...

        /** @var ProcessManager $manager */
        $manager = Injector::inst()->create(ProcessManager::class);
        $manager->setWorker($this->getCliScriptPath() . " dev/tasks/ProcessJobQueueChildTask");

        $logPath = Environment::getEnv('SS_DOORMAN_LOGPATH');
        if ($logPath) {
            $manager->setLogPath($logPath);
        }

        // Assign default rules
        $defaultRules = $this->getDefaultRules();
        if ($defaultRules) {
            foreach ($defaultRules as $rule) {
                $manager->addRule($rule);
            }
        }

        $descriptor = $this->getNextJobDescriptorWithoutMutex($queue);

        while ($manager->tick() || $descriptor) {
            if (QueuedJobService::singleton()->isMaintenanceLockActive()) {
                $service->getLogger()->info('Skipped queued job descriptor since maintenance log is active.');
                return;
            }

            $this->logDescriptorStatus($descriptor, $queue);

            if ($descriptor instanceof QueuedJobDescriptor) {
                $descriptor->JobStatus = QueuedJob::STATUS_INIT;
                $descriptor->write();

                $manager->addTask(new DoormanQueuedJobTask($descriptor));
            }

            sleep(1);

            $descriptor = $this->getNextJobDescriptorWithoutMutex($queue);
        }
    }
}
