<?php

namespace FullscreenInteractive\Source\Jobs;

use FullscreenInteractive\Source\Model\Notification;

/**
 * Base class for any notification job - email, push, sms
 *
 */
abstract class QueuedNotificationJob extends SourceQueuedJob
{
    private static $notificationId;

    public function __construct($notificationId = null)
    {
        if ($notificationId) {
            $this->currentStep = 0;
            $this->notificationId = $notificationId;
            $this->totalSteps = 1;
        }
    }

    public function setup()
    {
        parent::setup();

        if (!$this->getNotification()) {
            $this->isComplete = true;
            $this->addMessage('No notification to process');

            return;
        }

        $this->totalSteps = 1;
    }

    public function getNotification()
    {
        if (!$this->notificationId) {
            return false;
        }

        return Notification::get()->byId($this->notificationId);
    }

    public function process()
    {
        $this->currentStep = 1;
        $this->isComplete = true;

        parent::process();
    }

    public function getTitle()
    {
        return parent::getTitle() . ' (#'. $this->notificationId .')';
    }
}
