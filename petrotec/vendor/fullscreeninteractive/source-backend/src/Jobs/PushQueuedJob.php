<?php

namespace FullscreenInteractive\Source\Jobs;

use FullscreenInteractive\Source\Model\Notification;
use FullscreenInteractive\Source\Push\AirMessage;
use Symbiote\QueuedJobs\Services\AbstractQueuedJob;
use Symbiote\QueuedJobs\Services\QueuedJob;

/**
 * Process a push notification in a queue.
 *
 * The push notification data is stored as a notification object instance. This
 * instance stores information such as who needs to view the notification.
 */
class PushQueuedJob extends QueuedNotificationJob
{
    public function process()
    {
        parent::process();

        $notification = $this->getNotification();

        try {
            $air = new AirMessage($notification, $this);
            $air->deliver();
        } catch (\Exception $e) {
            $this->logException($e);

            $this->addMessage('Exception: '. $e->getMessage());
        }

        return;
    }
}
