<?php

namespace FullscreenInteractive\Source\Jobs;

use FullscreenInteractive\Source\Model\Notification;
use FullscreenInteractive\Source\Push\AirMessage;
use FullscreenInteractive\Source\Sms;
use Symbiote\QueuedJobs\Services\AbstractQueuedJob;
use Symbiote\QueuedJobs\Services\QueuedJob;

/**
 * Process a push notification in a queue.
 *
 * The push notification data is stored as a notification object instance. This
 * instance stores information such as who needs to view the notification.
 */
class SmsQueuedJob extends QueuedNotificationJob
{
    public function process()
    {
        parent::process();

        $notification = $this->getNotification();

        try {
            // @todo
            if ($notification) {
                foreach ($notification->Targets() as $target) {
                    $num = false;

                    foreach (['Mobile', 'MobilePhone', 'PhoneMobile', 'MobileNumber', 'Phone', 'PhoneNumber'] as $f) {
                        if ($target->{$f}) {
                            $num = $target->{$f};
                        }
                    }

                    if ($num) {
                        $message = $notification->SmsMessage;

                        if (!$message) {
                            $message = $notification->PushMessage;
                        }

                        if ($message) {
                            Sms::send($num, $message);
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            $this->logException($e);

            $this->addMessage('Exception: '. $e->getMessage());
        }

        return;
    }
}
