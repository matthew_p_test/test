<?php

namespace FullscreenInteractive\Source\Jobs;

use Symbiote\QueuedJobs\Services\AbstractQueuedJob;
use Symbiote\QueuedJobs\Services\QueuedJob;
use FullscreenInteractive\Source\Sentry;

abstract class SourceQueuedJob extends AbstractQueuedJob
{
    public function getJobType()
    {
        return QueuedJob::QUEUED;
    }

    public function getTitle()
    {
        return trim(str_replace(['FullscreenInteractive','\\'], ['','_'], get_class($this)));
    }

    public function logError($err)
    {
        Sentry::logError($err);
    }

    public function logException($ex)
    {
        if (!$ex instanceof \Exception) {
            return $this->logError($ex);
        }

        Sentry::logException($ex);

        return $this;
    }

    public function process()
    {
        // process task
    }
}
