<?php

namespace FullscreenInteractive\Source\Jobs;

use SilverStripe\Control\Email\Email;
use SilverStripe\View\ArrayData;
use Symbiote\QueuedJobs\Services\QueuedJob;
use SilverStripe\Core\Injector\Injector;
use FullscreenInteractive\Source\Controllers\AppController;
use SilverStripe\ORM\FieldType\DBField;

/**
 * Process a queued email notification instance.
 *
 */

class EmailQueuedJob extends QueuedNotificationJob
{
    public function process()
    {
        parent::process();

        $notification = $this->getNotification();

        $email = Email::create();
        $email->setSubject($notification->Subject);

        if ($notification->EmailTemplate) {
            $email->setHTMLTemplate($notification->EmailTemplate);
        } else {
            $email->setHTMLTemplate('Email\\Notification');
        }

        $email->setData(new ArrayData([
            'Title' => $notification->Subject,
            'Body' => DBField::create_field('HTMLText', ($notification->HTMLBody) ? $notification->HTMLBody : $notification->Body),
            'ApplicationName' => Injector::inst()->create(AppController::class)
                ->config()->get('ApplicationTitle'),
            'Notification' => $notification
        ]));

        foreach ($notification->Targets()->exclude('DisableEmailNotifications', 1) as $target) {
            if ($target->Email && Email::is_valid_address($target->Email)) {
                try {
                    $email->setTo($target->Email);
                    $email->send();
                } catch (\Exception $e) {
                    $this->logException($e);
                    $this->addMessage('Exception: '. $e->getMessage());
                }
            }
        }

        return;
    }
}
