<?php

namespace FullscreenInteractive\Source\Tasks;

use Symbiote\QueuedJobs\Services\QueuedJob;
use SilverStripe\Dev\BuildTask;

class RunJobQueueTask extends SourceTask
{
    /**
     * @config $max_expected_age
     *
     * Task should run at least every 5 minutes.
     */
    private static $max_expected_age = 300;

    /**
     * {@inheritDoc}
     * @var string
     */
    private static $segment = 'RunJobQueueTask';

    /**
     * @param HTTPRequest $request
     */
    public function run($request)
    {
        parent::run($request);

        $service = $this->getService();

        if ($request->getVar('list')) {
            // List helper
            $service->queueRunner->listJobs();

            return;
        }

        // Check if there is a job to run
        if (($job = $request->getVar('job')) && strpos($job, '-')) {
            // Run from a isngle job
            $parts = explode('-', $job);
            $id = $parts[1];
            $service->runJob($id);
            return;
        }

        // Run the queue
        $queue = $this->getQueue($request);
        echo 'running queue '. $queue;
        $service->runQueue($queue);
    }

    /**
     * Resolves the queue name to one of a few aliases.
     *
     * @todo Solve the "Queued"/"queued" mystery!
     *
     * @param HTTPRequest $request
     * @return string
     */
    protected function getQueue($request)
    {
        $queue = $request->getVar('queue');

        if (!$queue) {
            $queue = 'Queued';
        }

        switch (strtolower($queue)) {
            case 'immediate':
                $queue = QueuedJob::IMMEDIATE;
                break;
            case 'queued':
                $queue = QueuedJob::QUEUED;
                break;
            case 'large':
                $queue = QueuedJob::LARGE;
                break;
            default:
                break;
        }

        return $queue;
    }

    /**
     * Returns an instance of the QueuedJobService.
     *
     * @return QueuedJobService
     */
    public function getService()
    {
        return singleton('Symbiote\\QueuedJobs\\Services\\QueuedJobService');
    }
}
