<?php

namespace FullscreenInteractive\Source\Tasks\Util;

use SilverStripe\Security\Member;
use FullscreenInteractive\Source\Tasks\SourceTask;

class FindMemberTask extends SourceTask
{
    private static $segment = 'FindMember';

    private static $lock = false;

    public function run($request)
    {
        parent::run($request);

        if (!$request->getVar('q')) {
            echo "Missing request q";

            return;
        }

        $members = Member::get()->filterAny([
            'FirstName:StartsWith:nocase' => $request->getVar('q'),
            'Surname:StartsWith:nocase' => $request->getVar('q'),
            'Email:StartsWith:nocase' => $request->getVar('q')
        ]);

        if (!$members->count()) {
            echo "No results";

            return;
        }

        echo sprintf("+ %'-10s + %'-30s + %'-20s +". PHP_EOL, 'ID', 'Email', 'Name');

        foreach ($members as $member) {
            echo sprintf('| %10d | %30s | %20s |', $member->ID, $member->Email, $member->Name);
        }
    }
}
