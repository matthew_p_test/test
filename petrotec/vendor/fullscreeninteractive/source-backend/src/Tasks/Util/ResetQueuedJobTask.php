<?php

namespace FullscreenInteractive\Source\Tasks\Util;

use FullscreenInteractive\Source\Tasks\SourceTask;
use Symbiote\QueuedJobs\DataObjects\QueuedJobDescriptor;

class ResetQueuedJobTask extends SourceTask
{
    private static $segment = 'ResetQueuedJob';

    private static $touch_lock = false;

    public function run($request)
    {
        parent::run($request);

        if (!$request->getVar('ID')) {
            $this->echoError("Missing request ID");

            return;
        }

        $job = QueuedJobDescriptor::get()->byId($request->getVar('ID'));

        if (!$job) {
            $this->echoLine("Invalid job ID");

            return;
        }

        $job->JobStarted = null;
        $job->JobRestarted = null;
        $job->JobFinished = null;
        $job->StepsProcessed = 0;
        $job->LastProcessedCount = -1;
        $job->ResumeCounts = 0;
        $job->JobStatus = 'New';
        $job->write();

        $this->echoSuccess('Job #'. $job->ID . ' ('. $job->JobTitle .') has been refreshed and back on the grill.');
    }
}
