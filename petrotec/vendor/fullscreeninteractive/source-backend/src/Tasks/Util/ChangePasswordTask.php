<?php

namespace FullscreenInteractive\Source\Tasks\Util;

use SilverStripe\Security\Member;
use FullscreenInteractive\Source\Tasks\SourceTask;

class ChangePasswordTask extends SourceTask
{
    private static $segment = 'ChangePassword';

    private static $lock = false;

    public function run($request)
    {
        parent::run($request);

        $id = $request->getVar('ID');

        if (!$id) {
            $id = $request->getVar('id');
        }

        $email = $request->getVar('Email');

        if (!$email) {
            $email = $request->getVar('email');
        }

        if (!$email && !$id) {
            echo "Please provide an email or id var";
            die();
        }

        $members = null;
        $member = null;

        if ($id) {
            $field = 'ID';
            $member = Member::get()->byId($id);
        } else {
            $members = Member::get()->filterAny([
                'Email' => $email,
                'Username' => $email
            ]);

            $field = 'Email';

            if ($members->count() > 1) {
                echo 'Warning! multiple members with that email.';
            }
        }

        if (!$members) {
            $members = [$member];
        }

        foreach ($members as $member) {
            if (!$password = $request->getVar('password')) {
                $password = $member->generatePassword(8);
            }

            $member->registerSuccessfulLogin();
            $member->changePassword($password);
            $member->afterMemberLoggedIn();

            $this->echoSuccess('Updated member record #'.$member->ID . ' username: '. $member->Username .'to password '. $password);
        }
    }
}
