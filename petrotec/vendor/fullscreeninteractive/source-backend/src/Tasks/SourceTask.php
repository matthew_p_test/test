<?php

namespace FullscreenInteractive\Source\Tasks;

use SilverStripe\Dev\BuildTask;
use SilverStripe\Control\Director;
use FullscreenInteractive\Source\Sentry;

/**
 * Base class for managing Source Tasks - better reporting for error reporting
 * and tracking for when the task last ran.
 */
class SourceTask extends BuildTask
{
    /**
     * @config $max_expected_age
     *
     * The expected max delay for this task to be run. For instance - if you
     * have a task to run every day then the max_expected_age should be set to
     * (60 * 60 * 24) + 2 (24 hours + buffer).
     */
    private static $max_expected_age = false;

    /**
     * @config $touch_lock
     */
    private static $touch_lock = true;

    /**
     *
     */
    public function run($request)
    {
        if ($this->config()->touch_lock) {
            $this->touchLockFile();
        }
    }

    /**
     * Update the modified date of the lock file.
     *
     * @return $this
     */
    public function touchLockFile()
    {
        @mkdir(ASSETS_PATH .'/.sourcetasks/');
        file_put_contents($this->getLockFilePath(), date('d-m-Y H:i:s'));

        return $this;
    }

    /**
     * Returns the path to lock file. This file gets touched when a task starts.
     *
     * @return string
     */
    public function getLockFilePath()
    {
        // touch a test file with the latest change
        $class = str_replace('\\', '_', get_class($this));

        return ASSETS_PATH .'/.sourcetasks/'. $class . '.run';
    }

    /**
     * @param string $line
     *
     * @return $this
     */
    protected function echoLine($line)
    {
        echo $line . $this->eol();

        return $this;
    }

    /**
     * @param Exception $ex
     *
     * @return $this
     */
    protected function echoException($ex)
    {
        if (!$ex instanceof Exception) {
            return $this->echoError($ex);
        }

        Sentry::logException($ex);

        echo trim($ex->getMessage()) . $this->eol();

        return $this;
    }

    /**
     * @param string $message
     *
     * @return $this
     */
    protected function echoError($message)
    {
        Sentry::log($message);

        echo trim($message) . $this->eol();

        return $this;
    }

    /**
     * @param string $message
     *
     * @return $this
     */
    protected function echoSuccess($message)
    {
        echo trim($message) . $this->eol();

        return $this;
    }

    /**
     * Helper function to correctly output EOL based on platform and the current
     * interface.
     *
     * @return string
     */
    protected function eol()
    {
        return (Director::is_cli()) ? PHP_EOL : '<br />';
    }
}
