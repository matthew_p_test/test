<?php

namespace FullscreenInteractive\Source\Tasks\Diagnostic;

use SilverStripe\Logging\DetailedErrorFormatter;
use SilverStripe\Core\Injector\Injector;
use FullscreenInteractive\Source\Tasks\SourceTask;
use \Exception;

class RenderError extends SourceTask
{
    private static $segment = 'RenderError';

    public function run($request)
    {
        $format = Injector::inst()->create(DetailedErrorFormatter::class);

        $output = $format->format(['context' => ['exception' => new Exception('Test Exception')]]);
        var_dump($output);
        die();
    }
}
