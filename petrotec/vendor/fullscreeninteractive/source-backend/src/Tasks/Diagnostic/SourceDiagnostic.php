<?php

namespace FullscreenInteractive\Source\Tasks\Diagnostic;

use SilverStripe\ORM\DB;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\Control\Email\Email;
use SilverStripe\Control\Director;
use FullscreenInteractive\Source\Tasks\SourceTask;
use FullscreenInteractive\Source\Controllers\AppController;
use SilverStripe\Control\Email\Mailer;

/**
 * Delivering joy to the world
 */
class SourceDiagnostic extends SourceTask
{
    private static $segment = 'SourceDiagnostic';

    private static $lock_file = false;

    public function run($request)
    {
        printf('+----- SYSTEM '. $this->eol());
        printf('| PHP Version: %s' . PHP_EOL, PHP_VERSION_ID);
        printf('| PHP Date: %s' . PHP_EOL, date('Y-m-d H:i:s'));
        printf('| Base URL: %s' . PHP_EOL, Director::absoluteBaseURL());
        printf('| MySQL Current Time: %s'. PHP_EOL, DB::query('SELECT NOW()')->value());
        printf('| Mailer %s'. PHP_EOL, get_class(Injector::inst()->get(Mailer::class)));

        $email = Injector::inst()->create(Email::class);
        $email->setSubject('Test Mail');
        $email->setTo('will@fullscreen.io');
        $email->setFrom(Email::config()->get('send_all_emails_from'));
        $email->setData([
            'Body' => 'Output Test',
            'ApplicationName' => Injector::inst()->create(AppController::class)
                ->config()->get('ApplicationTitle')
        ]);

        $email->setHTMLTemplate('Email\\Notification');
        $sent = $email->send();

        var_dump($sent);
    }
}
