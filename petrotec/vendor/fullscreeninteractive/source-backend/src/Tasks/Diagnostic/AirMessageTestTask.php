<?php

namespace FullscreenInteractive\Source\Tasks\Diagnostic;

use SilverStripe\Dev\BuildTask;
use FullscreenInteractive\Source\Push\AirMessage;
use FullscreenInteractive\Source\Model\Notification;
use FullscreenInteractive\Source\Model\DeviceToken;
use SilverStripe\ORM\DB;
use SilverStripe\Control\Director;

/**
 * Delivering joy to the world
 */
class AirMessageTestTask extends BuildTask
{
    private static $segment = 'AirMessageTest';

    public function run($request)
    {
        $notificationId = $request->requestVar('notificationId');
        $tokenId = $request->requestVar('tokenId');

        if (!$tokenId) {
            echo 'missing tokenId';

            return;
        }

        if ($notificationId) {
            $notification = Notification::get()->byId($notificationId);
        } else {
            $notification = new Notification();
            $notification->PushTitle = 'Hello';
            $notification->PushMessage = 'Welcome to our app.';
        }

        $token = DeviceToken::get()->byId($tokenId);

        if (!$token) {
            echo 'missing token in database.';

            return;
        }

        if (!$notification) {
            echo 'invalid notification';

            return;
        }

        if (!$env = $request->requestVar('env')) {
            $env = (Director::isLive()) ? 'prod' : 'dev';
        }

        $air = new AirMessage($notification, null);

        if ($token->Platform === 'iOS') {
            return $air->deliverIos([
                $air->wrapDeviceToken($token)
            ], $env);
        } else {
            return $air->deliverAndroid([
                $air->wrapDeviceToken($token)
            ], $env);
        }
    }
}
