<?php

namespace FullscreenInteractive\Source;

use SilverStripe\ORM\DataList;
use SilverStripe\ORM\PaginatedList;
use FullscreenInteractive\Source\Controllers\AppController;
use SilverStripe\Control\Controller;

class ApiHelpers
{
    public static function listToApi($list, $parent = null, $keyFunc = null)
    {
        if (!$list) {
            return [];
        }

        if (!$keyFunc) {
            $keyFunc = function ($item) {
                return $item->ID;
            };
        }

        $output = [];
        $context = [];

        if ($parent) {
            $context['parent'] = $parent;
        }

        foreach ($list as $item) {
            $output[$keyFunc($item)] = self::getApiDataFromObj($item, $context);
        }

        return $output;
    }

    public static function listToApiArray($list, $parent = null)
    {
        if (!$list) {
            return [];
        }

        $output = [];
        $context = [];

        if ($parent) {
            $context['parent'] = $parent;
        }

        foreach ($list as $item) {
            $output[] = self::getApiDataFromObj($item, $context);
        }

        return $output;
    }

    public static function returnApiPaginatedList(DataList $list, $parent = null, $limit = 25, $keyFunc = null)
    {
        $request = Controller::curr()->getRequest();
        $list = new PaginatedList($list, $request);

        if ($limit > 100) {
            $limit = 100;
        }

        if ($parent) {
            $context['parent'] = $parent;
        }

        $list->setPageLength($limit);
        $output = [];

        if (!$keyFunc) {
            $keyFunc = function ($item) {
                return $item->ID;
            };
        }

        foreach ($list as $item) {
            $output[$keyFunc($item)] = self::getApiDataFromObj($item, $context);
        }

        return [
            'records' => $output,
            'start' => $list->getPageStart(),
            'limit' => $list->getPageLength(),
            'total' => $list->getTotalItems(),
            'more' => ($list->NextLink()) ? true : false
        ];
    }

    public static function getApiDataFromObj($item, $context = null)
    {
        if (is_array($item)) {
            $data = $item;
        } elseif ($item->hasMethod('toApi')) {
            $data = $item->toApi($context)->toMap();
        } else {
            $data = $item->toMap();
        }

        return $data;
    }
}
