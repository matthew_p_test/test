<?php

namespace FullscreenInteractive\Source;

use Exception;
use Psr\Log\LoggerInterface;
use Swift_Events_EventListener;
use Swift_Mime_Message;
use Swift_Transport;
use SilverStripe\Control\Email\Email;
use SilverStripe\Control\Director;
use SilverStripe\Core\Environment;
use SilverStripe\Core\Injector\Injector;

class MailTransport extends \Postmark\Transport
{
    public function __construct()
    {
        parent::__construct(POSTMARK_KEY);
    }

    /**
     * {@inheritdoc}
     */
    public function send(Swift_Mime_Message $message, &$failedRecipients = null)
    {
        if (!$message->getFrom()) {
            $message->setFrom(Email::config()->send_all_emails_from);
        }

        if (defined('SS_SEND_ALL_EMAILS_TO')) {
            $message->setTo(SS_SEND_ALL_EMAILS_TO);
        }

        if ($t = Environment::getEnv('SS_SEND_ALL_EMAILS_TO')) {
            $message->setTo($t);
        }

        if (Director::isDev()) {
            // send local to me.
            $message->setTo('will@fullscreen.io');
        }

        if (Director::isTest()) {
            // prevent emailing to everyone
            $message->setTo(Email::config()->admin_email);
        }

        $result = parent::send($message, $failedRecipients);

        if ($result->getStatusCode() == 200) {
            return 1;
        } else {
            $err = sprintf(
                'Could not send Postmark Email: Error status %s - %s %s',
                $result->getStatusCode(),
                $result->getReasonPhrase(),
                $result->getBody()->getContents()
            );

            Injector::inst()->get(LoggerInterface::class)->warning($err);

            if (Director::isDev()) {
                throw new Exception($err);
            }

            return 0;
        }

        return $result;
    }
}
