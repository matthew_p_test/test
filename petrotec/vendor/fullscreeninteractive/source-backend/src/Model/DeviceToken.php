<?php

namespace FullscreenInteractive\Source\Model;

use SilverStripe\ORM\DataObject;

class DeviceToken extends DataObject
{
    private static $db = [
        'Token' => 'Varchar(255)',
        'Platform' => 'Enum("iOS, Android")',
        'Environment' => 'Enum("dev, prod", "prod")'
    ];

    private static $has_one = [
        'Member' => 'SilverStripe\Security\Member'
    ];

    private static $table_name = 'DeviceToken';

    public function canCreate($member = null, $context = [])
    {
        return false;
    }

    public function canView($member = null)
    {
        return false;
    }
}
