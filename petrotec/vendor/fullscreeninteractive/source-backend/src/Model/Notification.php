<?php

namespace FullscreenInteractive\Source\Model;

use SilverStripe\ORM\DataObject;
use SilverStripe\Security\Security;
use SilverStripe\Security\Permission;
use SilverStripe\View\ArrayData;
use SilverStripe\Core\Injector\Injector;
use FullscreenInteractive\Source\ApiReadable;
use Symbiote\QueuedJobs\Services\QueuedJobService;
use FullscreenInteractive\Source\Jobs\PushQueuedJob;
use FullscreenInteractive\Source\Jobs\EmailQueuedJob;
use FullscreenInteractive\Source\Jobs\SmsQueuedJob;
use SilverStripe\Control\Email\Email;
use SilverStripe\Control\Controller;
use SilverStripe\Control\Director;
use SilverStripe\Security\SecurityToken;
use SilverStripe\ORM\DB;

/**
 * A notification that is sent to the user. Notifications can be sent queued as
 * emails, push notifications or in-app messages.
 */

class Notification extends DataObject implements ApiReadable
{
    private static $db = [
        'PushNotification' => 'Boolean',
        'PushTitle' => 'Varchar(255)',
        'PushMessage' => 'Varchar(255)',
        'DeliverInsideApp' => 'Boolean',
        'EmailNotification' => 'Boolean',
        'Subject' => 'Varchar(255)',
        'Body' => 'Text',
        'ScheduledTime' => 'Datetime',
        'HTMLBody' => 'HTMLText',
        'TargetLink' => 'Varchar(255)',
        'EmailTemplate' => 'Varchar',
        'CustomData' => 'Text',
        'SmsMessage' => 'Boolean'
    ];

    private static $has_one = [
        'Author' => 'SilverStripe\Security\Member'
    ];

    private static $many_many = [
        'Targets' => 'SilverStripe\Security\Member'
    ];

    private static $many_many_extraFields = [
        'Targets' => [
            'Read' => 'Boolean',
            'Deleted' => 'Boolean',
            'PushSuccess' => 'Boolean',
            'EmailSuccess' => 'Boolean',
            'ReadDate' => 'Datetime'
        ]
    ];

    private static $table_name = 'Notification';

    public function onBeforeWrite()
    {
        if (!$this->ScheduledTime) {
            $this->ScheduledTime = strtotime('-1 MINUTE');
        }

        parent::onBeforeWrite();
    }

    /**
     * Sends the notification out across email and push.
     *
     */
    public function send()
    {
        if ($this->PushMessage) {
            $job = Injector::inst()->create(PushQueuedJob::class, $this->ID);

            singleton(QueuedJobService::class)->queueJob(
                $job,
                $this->ScheduledTime,
                $this->AuthorID
            );
        }

        if ($this->EmailNotification) {
            $job = Injector::inst()->create(EmailQueuedJob::class, $this->ID);

            singleton(QueuedJobService::class)->queueJob(
                $job,
                $this->ScheduledTime,
                $this->AuthorID
            );
        }

        if ($this->SmsMessage) {
            $job = Injector::inst()->create(SmsQueuedJob::class, $this->ID);

            singleton(QueuedJobService::class)->queueJob(
                $job,
                $this->ScheduledTime,
                $this->AuthorID
            );
        }

        return true;
    }

    /**
     * @param SilverStripe\Security\Member
     *
     * @return boolean
     */
    public function canView($member = null)
    {
        if (!$member) {
            $member = Security::getCurrentUser();
        }

        if ($member) {
            if ($this->AuthorID === $member->ID) {
                return true;
            }

            if ($this->Targets()->find('ID', $member->ID)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return boolean
     */
    public function canDelete($member = null)
    {
        if (!$member) {
            $member = Security::getCurrentUser();
        }

        if ($member) {
            if ($this->Targets()->find('ID', $member->ID)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param SilverStripe\Security\Member
     *
     */
    public function canEdit($member = null)
    {
        if (!$member) {
            $member = Security::getCurrentUser();
        }

        if (!$member) {
            return false;
        }

        return (Permission::checkMember($member, 'ADMIN') || $member->ID === $this->AuthorID);
    }

    /**
     * @param array
     *
     * @return ArrayData
     */
    public function toApi($context = [])
    {
        $read = false;
        $readDate = null;

        if ($member = Security::getCurrentUser()) {
            if ($target = $this->Targets()->find('MemberID', $member->ID)) {
                $read = $target->Read;
                $readDate = strtotime($target->ReadDate);
            }

            if ($member->ID == $this->AuthorID) {
                $read = true;
            }
        }

        $data = new ArrayData([
            'id' => $this->ID,
            'subject' => (string) $this->Subject,
            'read' => (boolean) $read,
            'author' => $this->Author()->toApi(['parent' => $this])->toMap(),
            'read_date' => $readDate,
            'date' => $this->dbObject('ScheduledTime')->getTimestamp(),
            'message' => (string) ($this->PushMessage) ? $this->PushMessage : $this->Body,
            'title' => (string) ($this->PushTitle) ? $this->PushTitle : $this->Subject
        ]);

        return $data;
    }

    /**
     * Returns a list of all the device tokens that should receive this
     * notification
     *
     * @return array
     */
    public function getActiveDeviceTokens()
    {
        $tokens = [];

        foreach ($this->Targets() as $target) {
            if (!$target->DisablePushNotifications) {
                foreach ($target->DeviceTokens() as $token) {
                    $tokens[] = $token;
                }
            }
        }

        return $token;
    }

    /**
     * Web link to read notification. Once the user views this notification read
     * handler they will bounce to the destination `Link` or it will take them
     * back to the dashboard.
     *
     * @param string $action
     *
     * @return string
     */
    public function Link($action = 'read')
    {
        return Controller::join_links(
            Director::absoluteBaseURL(),
            'notifications/',
            $action,
            $this->ID,
            md5($this->ID .__CLASS__) .'?SecurityID='.SecurityToken::inst()->getValue()
        );
    }

    /**
     * Marks a notification as read.
     *
     * @return boolean
     */
    public function markAsRead()
    {
        $member = Security::getCurrentUser();

        if ($member) {
            DB::query(sprintf(
                'UPDATE Notification_Targets SET `Read` = 1, ReadDate = NOW() WHERE MemberID = %s AND NotificationID = %s',
                $member->ID,
                $this->ID
            ));

            return true;
        }

        return false;
    }

    /**
     * Marks a notification as deleted.
     *
     * @return boolean
     */
    public function markAsDeleted()
    {
        $member = Security::getCurrentUser();

        if ($member) {
            DB::query(sprintf(
                'UPDATE Notification_Targets SET `Read` = 1, ReadDate = NOW(), `Deleted` = 1 WHERE MemberID = %s AND NotificationID = %s',
                $member->ID,
                $this->ID
            ));

            return true;
        }

        return false;
    }

    /**
     * Helper to mark whether a notification is unread
     *
     * @return boolean
     */
    public function isUnread()
    {
        $member = Security::getCurrentUser();

        if ($member) {
            if ($sent = $this->Targets()->find('ID', $member->ID)) {
                return !$sent->Read && !$sent->Deleted;
            }
        }

        return true;
    }

    /**
     * @return string
     */
    public function getReadTotal()
    {
        $total = $this->Targets()->count();
        $read = $this->Targets()->filter('Read', 1)->count();

        if ($read == $total) {
            return 'all';
        }

        return $read;
    }

     /**
      * @return Boolean
      */
    public function ShowReadTotal()
    {
        return ($this->AuthorID == Security::getCurrentUser()->ID);
    }
}
