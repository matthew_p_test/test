<?php

namespace FullscreenInteractive\Source\Logging;

use SilverStripe\Dev\DebugView;
use SilverStripe\Control\Director;

class Viewer extends DebugView
{
    public function renderHeader($httpRequest = null)
    {
        $url = htmlentities(
            $_SERVER['REQUEST_METHOD'] . ' ' . $_SERVER['REQUEST_URI'],
            ENT_COMPAT,
            'UTF-8'
        );

        $debugCSS = rtrim(RESOURCES_DIR, '/') .'/source-backend/css/debug.css';
        $output = '<!DOCTYPE html><html><head><title>' . $url . '</title>';
        $output .= '<link rel="stylesheet" type="text/css" href="'. $debugCSS .'" />';
        $output .= '</head>';
        $output .= '<body>';

        return $output;
    }
}
