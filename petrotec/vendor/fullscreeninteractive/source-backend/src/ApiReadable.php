<?php

namespace FullscreenInteractive\Source;

interface ApiReadable
{
    public function toApi($context = []);
}
