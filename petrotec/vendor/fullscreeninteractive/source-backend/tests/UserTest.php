<?php

namespace FullscreenInteractive\Source\Tests;

use SilverStripe\Security\Member;
use FullscreenInteractive\Source\Tests\EndpointTest;
use FullscreenInteractive\Source\Model\Notification;

class UserTest extends EndpointTest
{
    protected static $fixture_file = 'source.yml';

    public function testLogin()
    {
        $response = $this->call(
            'api/user/login',
            null,
            [
            'username' => 'will@fullscreen.io',
            'password' => 'pass1*'
            ]
        );

        $this->assertTrue($response->isSuccess());
        $this->assertTrue($response->getBody()['first_name'] === 'Will');

        $response = $this->call(
            'api/user/login',
            null,
            [
            'username' => 'will@fullscreen.io',
            'password' => 'no pass'
            ]
        );

        $this->assertTrue($response->is400());

        $response = $this->call(
            'api/user/login',
            null,
            [
            'username' => '',
            'password' => ''
            ]
        );

        $this->assertTrue($response->is400());
    }


    public function testLoginWorksWithEmail()
    {
        $member = $this->objFromFixture(Member::class, 'quiet');
        $response = $this->call(
            'api/user/login',
            null,
            [
            'username' => 'quiet',
            'password' => 'silent'
            ]
        );

        $this->assertTrue($response->isSuccess());
        $this->assertTrue($response->getBody()['first_name'] === 'quiet');

        $response = $this->call(
            'api/user/login',
            null,
            [
            'username' => 'john@fullscreen.io',
            'password' => 'silent'
            ]
        );

        $this->assertTrue($response->isSuccess());
        $this->assertTrue($response->getBody()['first_name'] === 'quiet');
    }

    public function testRegister()
    {
        $response = $this->call('api/user/register', null);
        $this->assertTrue($response->is400());

        $response = $this->call('api/user/register', null, [
            'email' => 'testuser@fullscreen.io',
            'first_name' => 'john',
            'last_name' => 'smith'
        ]);

        $this->assertTrue($response->isSuccess());
        $this->assertTrue($response->getBody()['first_name'] === 'john');

        $response = $this->call('api/user/register', null, [
            'email' => 'testuser@fullscreen.io',
            'first_name' => 'john',
            'last_name' => 'smith'
        ]);

        $this->assertTrue($response->is400());

        $response = $this->call('api/user/register', null, [
            'email' => 'testuser2@fullscreen.io',
            'first_name' => 'john',
            'last_name' => 'smith',
            'password' => 'borris'
        ]);

        $this->assertTrue($response->isSuccess());
        $this->assertTrue($response->getBody()['first_name'] === 'john');

        $response = $this->call('api/user/login', null, [
            'username' => 'testuser2@fullscreen.io',
            'password' => 'borris'
        ]);

        $this->assertTrue($response->isSuccess());
        $this->assertTrue($response->getBody()['first_name'] === 'john');
    }

    public function testFetch()
    {
        $member = $this->objFromFixture(Member::class, 'finn');

        $response = $this->call('api/user/fetch/'. $member->ID, $member);

        $this->assertTrue($response->isSuccess());
        $this->assertTrue($response->getBody()['session_token'] === $member->MobileSessionToken);
    }

    public function testForgot()
    {
        $response = $this->call('api/user/forgot', null, [
            'emailOrUserName' => 'will@fullscreen.io'
        ]);

        $this->assertTrue($response->isSuccess());
        $this->assertEmailSent('will@fullscreen.io', null, 'Your password reset link');
    }

    public function testSavePicture()
    {
         $this->markTestIncomplete();
    }

    public function testGetNotifications()
    {
        $member = $this->objFromFixture(Member::class, 'finn');

        $this->assertEquals(2, $member->getNotifications()->count());

        // delete one of the notifications and it should filter
        $testBasicEmailNotification =  $this->objFromFixture(Notification::class, 'testBasicEmailNotification');
        $testBasicEmailNotification->Targets()->remove($member);

        $testBasicEmailNotification->Targets()->add($member, [
            'Deleted' => 1
        ]);

        $this->assertEquals(1, $member->getNotifications()->count());
    }


    public function testGetUnreadNotifications()
    {
        $member = $this->objFromFixture(Member::class, 'finn');

        $this->assertEquals(2, $member->getUnreadNotifications()->count());

        // delete one of the notifications and it should filter
        $testBasicEmailNotification =  $this->objFromFixture(Notification::class, 'testBasicEmailNotification');
        $testBasicEmailNotification->Targets()->remove($member);

        $testBasicEmailNotification->Targets()->add($member, [
            'Read' => 1
        ]);

        $this->assertEquals(1, $member->getUnreadNotifications()->count());

        // delete the one of the notifications and it should filter
        $testNotification =  $this->objFromFixture(Notification::class, 'testPushNotification');
        $testNotification->Targets()->remove($member);

        $testNotification->Targets()->add($member, [
            'Read' => 1
        ]);

        $this->assertEquals(0, $member->getUnreadNotifications()->count());
    }

    public function testChangePassword()
    {
        $member = $this->objFromFixture(Member::class, 'finn');

        $response = $this->call('api/user/changepassword', $member, [
            'newPassword' => 'johnboypass',
            'password' => 'finnlay2017'
        ]);

        $this->assertTrue($response->isSuccess());
        $this->assertTrue($response->getBody()['first_name'] === 'Finn');
        $member = $this->objFromFixture(Member::class, 'finn');

        $response = $this->call('api/user/login', null, [
            'username' => 'finntest@fullscreen.io',
            'password' => 'johnboypass'
        ]);

        $this->assertTrue($response->isSuccess());
        $this->assertTrue($response->getBody()['first_name'] === 'Finn');
    }
}
