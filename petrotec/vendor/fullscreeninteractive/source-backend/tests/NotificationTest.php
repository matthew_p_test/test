<?php

namespace FullscreenInteractive\Source\Tests;

use SilverStripe\Security\Member;
use FullscreenInteractive\Source\Model\Notification;

class NotificationTest extends EndpointTest
{
    protected static $fixture_file = 'source.yml';

    public function testFetch()
    {
        $member = $this->objFromFixture(Member::class, 'will');

        $notification = new Notification();
        $notification->Targets()->add($member->ID);
        $notification->DeliverInsideApp = 1;
        $notification->ScheduledTime = strtotime('-1 HOUR');
        $notification->Subject = 'Test Notification';
        $notification->write();

        $response = $this->call('api/notifications/fetch', $member);
        $this->assertTrue($response->isSuccess());
        $this->assertTrue($response->hasRecord('subject', 'Test Notification'));
    }

    public function testRead()
    {
        $member = $this->objFromFixture(Member::class, 'will');
        $other = $this->objFromFixture(Member::class, 'finn');

        $notification = new Notification();
        $notification->Targets()->add($member->ID);
        $notification->Subject = 'Test Notification';
        $notification->DeliverInsideApp = 1;
        $notification->ScheduledTime = strtotime('-1 HOUR');
        $notification->write();

        $response = $this->call('api/notifications/read/'. $notification->ID, $other);
        $this->assertTrue($response->is404());

        $response = $this->call('api/notifications/read/a', $other);
        $this->assertTrue($response->is404());

        $response = $this->call('api/notifications/read/'. $notification->ID, $member);
        $this->assertTrue($response->isSuccess());
        $this->assertTrue($response->hasRecord('subject', 'Test Notification'));
    }

    public function testStatuses()
    {
        $author = $this->objFromFixture(Member::class, 'will');
        $member = $this->objFromFixture(Member::class, 'finn');

        $notification = new Notification();
        $notification->Targets()->add($member->ID);
        $notification->Subject = 'Test Notification';
        $notification->AuthorID = $author->ID;
        $notification->DeliverInsideApp = 1;
        $notification->ScheduledTime = strtotime('-1 HOUR');
        $notification->write();

        $response = $this->call('api/notifications/statuses/'. $notification->ID, $author);
        $this->assertTrue($response->isSuccess());
    }

    public function testDelete()
    {
        $member = $this->objFromFixture(Member::class, 'will');
        $other = $this->objFromFixture(Member::class, 'finn');

        $notification = new Notification();
        $notification->Targets()->add($member->ID);
        $notification->write();

        $response = $this->call('api/notifications/delete/'. $notification->ID, $other);
        $this->assertTrue($response->is404());

        $response = $this->call('api/notifications/delete/a', $other);
        $this->assertTrue($response->is404());

        $response = $this->call('api/notifications/delete/'. $notification->ID, $member);
        $this->assertTrue($response->isSuccess());
    }
}
