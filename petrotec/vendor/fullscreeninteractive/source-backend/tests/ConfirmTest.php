<?php

namespace FullscreenInteractive\Source\Tests;

use SilverStripe\Security\Member;
use SilverStripe\Security\Security;
use SilverStripe\Dev\FunctionalTest;

class ConfirmTest extends FunctionalTest
{
    protected static $fixture_file = 'source.yml';

    public function testAccount()
    {
        $member = $this->objFromFixture(Member::class, 'will');
        $member->ConfirmedAccount = 0;
        $member->ConfirmToken = '1234';
        $member->write();

        $response = $this->get('confirm/account/'. $member->ID.'/1234');

        $this->assertEquals(200, $response->getStatusCode());

        $member = Member::get()->byId($member->ID);

        $this->assertEquals(1, $member->ConfirmedAccount);
        $this->assertNull($member->ConfirmToken);

        // member should be logged in
        $this->assertEquals($member->ID, Security::getCurrentUser()->ID);

        Security::setCurrentUser(null);

        // log the current user out and try confirm the account again. It should
        // not show an error
        // try to
        $response = $this->get('confirm/account/'. $member->ID.'/12345');
        $this->assertEquals(200, $response->getStatusCode());

        $response = $this->get('confirm/account/21389341083147931491734/12345');
        $this->assertEquals(404, $response->getStatusCode());

        $response = $this->get('confirm/account');
        $this->assertEquals(400, $response->getStatusCode());
    }
}
