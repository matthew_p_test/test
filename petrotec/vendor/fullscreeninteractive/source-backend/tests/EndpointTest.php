<?php

namespace FullscreenInteractive\Source\Tests;

use SilverStripe\Dev\FunctionalTest;
use SilverStripe\Core\Environment;

class EndpointTest extends FunctionalTest
{
    /**
     * Makes an authenticated call to the Source API
     */
    public function call($url, $member = null, $data = null)
    {
        $headers = [
            'Secure-Token' => Environment::getEnv('MOBILE_APP_TOKEN')
        ];

        if ($member) {
            if (!$member->MobileSessionToken) {
                $member->registerMobileLogin();
            }

            $headers['Secure-Token-Session'] = $member->MobileSessionToken;
        }

        if ($data) {
            return new EndpointTestResponse($this->post($url, $data, $headers));
        } else {
            return new EndpointTestResponse($this->get($url, null, $headers));
        }
    }

    public function testApiPing()
    {
        $this->assertTrue($this->call('api/ping')->isSuccess());
    }
}
