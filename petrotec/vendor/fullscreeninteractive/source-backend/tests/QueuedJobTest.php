<?php

namespace FullscreenInteractive\Source\Tests;

use SilverStripe\Security\Member;
use SilverStripe\Dev\SapphireTest;
use FullscreenInteractive\Source\Model\Notification;
use FullscreenInteractive\Source\Jobs\EmailQueuedJob;
use FullscreenInteractive\Source\Jobs\PushQueuedJob;

class QueuedJobTest extends SapphireTest
{
    protected static $fixture_file = 'source.yml';

    public function testEmailQueuedJob()
    {
        $notification = $this->objFromFixture(Notification::class, 'testBasicEmailNotification');

        $email = new EmailQueuedJob($notification->ID);
        $email->setup();

        $this->assertEquals($notification->ID, $email->getNotification()->ID);
        $email->process();

        $this->assertEmailSent('finntest@fullscreen.io', null, 'Test Notification For Finn');
    }

    public function testPushQueuedJob()
    {
        $notification = $this->objFromFixture(Notification::class, 'testPushNotification');

        $push = new PushQueuedJob($notification->ID);
        $push->setup();

        $this->assertEquals($notification->ID, $push->getNotification()->ID);

        // process job @todo mock delivery
        $push->process();
    }
}
