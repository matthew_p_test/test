<?php

namespace FullscreenInteractive\Source\Tests;

use SilverStripe\Control\HTTPResponse;

class EndpointTestResponse
{
    private $response;

    public function __construct(HTTPResponse $response)
    {
        $this->response = $response;
    }

    public function getBody()
    {
        return json_decode($this->response->getBody(), true);
    }

    public function isSuccess()
    {
        return ($this->response->getStatusCode() == 200 && is_array($this->getBody()));
    }

    public function is404()
    {
        return $this->response->getStatusCode() == 404;
    }

    public function is400()
    {
        return $this->response->getStatusCode() == 400;
    }

    public function is403()
    {
        return $this->response->getStatusCode() == 403;
    }

    public function messageContains($str)
    {
        return strpos($this->getBody()['message'], $str) !== false;
    }

    public function getStatusCode()
    {
        return $this->response->getStatusCode();
    }

    public function hasRecord($field, $value)
    {
        $body = $this->getBody();

        if (isset($body['records'])) {
            foreach ($body['records'] as $record) {
                if (isset($record[$field]) && $record[$field] == $value) {
                    return true;
                }
            }
        } else {
            if (isset($body[$field]) && $body[$field] == $value) {
                return true;
            }
        }

        return false;
    }
}
