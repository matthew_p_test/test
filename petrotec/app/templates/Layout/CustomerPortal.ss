<header class="header-icons overlay" style="padding: 20px 0">
	<div class="container align-bottom">
		<div class="row">
			<div class="col-sm-12 col-md-8 <% if not LeftPlease %>col-md-offset-2<% end_if %>">
				<% if FilterForm %>
				<div class="filters">
					$FilterForm
				</div>
				<% end_if %>
			</div>
		</div><!--end of row-->
	</div><!--end of container-->
</header>

<section class="article-single small-pad-top">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-6 col-md-offset-2">
				<div class="file-search">
					<input type="text" class="filter-files" placeholder="Search for file.." />
					<i class="icon_search"></i>
				</div>

				<div id="file-list">
					<ul class="list">
						<% if Folders %>
							<% loop Folders %>
								<li class="folder" data-folder-id="$ID" data-customer="$CustomerID">
									<div class="title" style="display: none"><% loop Files %>$Title, $Description<% end_loop %></div>
									<div class="list--folder">
										<i class="icon-folder-o"></i>
										<i class="icon-folder-open"></i>
										$Title
									</div>

									<div class="list--folderfiles" style="display: none">
										<ul>
											<% loop Files %>
												<li><% include CustomerPortalFile %></li>
											<% end_loop %>
										</ul>
									</div>
								</li>
							<% end_loop %>
						<% end_if %>
						<% loop Uploads.Sort(Title, ASC) %>
							<li data-customer="$CustomerID"><% include CustomerPortalFile %></li>
						<% end_loop %>
					</ul>
				</div>
			</div>

			<div class="col-sm-12 col-md-3 col-md-offset-1">
				<div class="widget">
					<div class="widget--header">
						<h2>Bookmarked Files</h2>
					</div>

					<div class="bookmarks">
						<% if CurrentMember.BookmarkedFiles %>
							<% loop CurrentMember.BookmarkedFiles.Sort(Title, ASC) %>
								<div class="file-minor">
									<h3><a href="$LocalDownloadLink" target="_blank">$Title</a></h3>
									<% if Description %><p>$Description</p><% end_if %>
									<p><small>Last updated: $LastEdited.Nice</small></p>
								</div>
							<% end_loop %>
						<% else %>
							<p>You currently have no bookmarked files. To save a bookmark to a file select the star icon</p>
						<% end_if %>
					</div>
				</div>
		</div><!--end of row-->
	</div><!--end of container-->

	<% include BackButton %>
</section>
