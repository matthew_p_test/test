<header class="header-icons overlay" style="padding: 20px 0">
		
	<% if not HideText %>
	<div class="container align-bottom">
		<div class="row">
			<div class="col-sm-12">
				<% if Parent %><span class="text-white alt-font"><% if Parent.Parent %><a href="$Parent.Parent.Link">$Parent.Parent.Title</a> &rsaquo; <% end_if %><a href="$Parent.Link">$Parent.Title</a></span><% end_if %>
				<% if FilterForm %>
				<div class="filters">
					$FilterForm
				</div>
				<% end_if %>
			</div>
		</div><!--end of row-->
	</div><!--end of container-->
	<% end_if %>
</header>

<section class="no-pad-bottom projects-gallery first-child">
	<div class="projects-wrapper clearfix">
		<div class="container">
			<div class="row">
				<div class="projects-container column-projects">	
					<% if AllChildren %>
						<% loop AllChildren %>
							<div class="col-md-4 col-sm-6 project <% if Related %><% loop Related %>cat-Form_FilterForm_Services-{$ID} <% end_loop %><% end_if %> <% if Categories %><% loop Categories %>cat-Form_FilterForm_Categories-{$ID} <% end_loop %><% end_if %> image-holder">
								<a href="$Link" class="project-inner">
									<div class="background-image-holder">
										<% if SortedPhotos %>
											<% with $SortedPhotos.First %>
												<img class="background-image" alt="Background Image" src="$ScaleWidth(400).URL" style="display: none;">
											<% end_with %>
										<% end_if %>
									</div>

									<div class="align-bottom">
										<h3 class="text-white"><strong>$Title</strong> $Parent.Title</h3>
									</div>

									<div class="btn btn-primary btn-white">See Project</div>
								</a>
							</div><!--end of individual project-->
						<% end_loop %>
					<% end_if %>
				</div><!--end of projects-container-->
			</div>
		</div>
	</div><!--end of projects wrapper-->
	
</section>