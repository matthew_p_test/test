<section class="no-pad login-page first-child">
	<div class="bg-secondary-1">
		<div class="container align-vertical" style="padding: 80px 0">
			<div class="row">
				<div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 text-center">
					<div class="photo-form-wrapper clearfix">
						$Form
					</div>
				</div>
			</div><!--end of row-->
		</div><!--end of container-->
	</div>
</section>