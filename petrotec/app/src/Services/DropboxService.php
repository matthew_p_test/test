<?php

use Kunnu\Dropbox\Dropbox;
use Kunnu\Dropbox\DropboxApp;
use Kunnu\Dropbox\Models\FolderMetadata;
use Psr\Log\LoggerInterface;
use SilverStripe\SiteConfig\SiteConfig;
use SilverStripe\Core\Config\Config;
use SilverStripe\Control\Controller;
use SilverStripe\Control\Director;
use SilverStripe\Core\Injector\Injector;

class DropboxService
{
    protected static $inst;

    protected $dropbox;

    protected $app;

    protected $active = [];

    protected $folderSort = 0;

    protected $objSort = 0;

    public static function inst()
    {
        if (!self::$inst) {
            self::$inst = new DropboxService();
        }

        return self::$inst;
    }

    public function __construct()
    {
        $this->app = new DropboxApp(
            Config::inst()->get('DropboxService', 'AppKey'),
            Config::inst()->get('DropboxService', 'AppSecret'),
            SiteConfig::current_site_config()->DropboxAccessToken
        );

        $this->dropbox = new Dropbox($this->app);
    }

    public function isValid()
    {
        return (
            Config::inst()->get('DropboxService', 'AppKey') &&
            Config::inst()->get('DropboxService', 'AppSecret') &&
            SiteConfig::current_site_config()->DropboxAccessToken
        );
    }

    public function getDropboxHelper()
    {
        return $this->dropbox;
    }

    public function getAuthHelper()
    {
        return $this->dropbox->getAuthHelper();
    }

    public function getAccessToken($code, $state)
    {
        $authHelper = $this->dropbox->getAuthHelper();
        $callbackUrl = Controller::join_links(Director::absoluteBaseURL() . "dropbox/callback/");

        return $authHelper->getAccessToken($code, $state, $callbackUrl);
    }

    /**
     * @return array
     */
    public function getFolders()
    {
        if (!$this->isValid()) {
            return [];
        }

        return DropboxFolder::get()->map('Path', 'Title');
    }

    /**
     * @return
     */
    public function syncFilesToFolder($folder, $customer)
    {
        $contents = $this->dropbox->listFolder($folder);

        $this->folderSort = 0;
        $this->objSort = 0;

        $this->importItems($contents, $customer);
        $this->cleanUpDeletedItems($customer);
    }

    public function cleanUpDeletedItems($customer)
    {
        if (!$customer || !$customer->ID) {
            return;
        }

        $totalFiles = 0;

        foreach ($this->active as $folderId => $files) {
            $totalFiles += count($files);

            $delete = CustomerFile::get()->filter([
                'ParentID' => $folderId,
                'CustomerID' => $customer->ID
            ]);

            if ($files) {
                $delete = $delete->exclude([
                    'DropboxID' => $files,
                    'Archived' => '1'
                ]);
            }

            foreach ($delete as $d) {
                if (in_array($d->DropboxID, $files)) {
                    continue;
                }

                echo sprintf("Archiving file #%s %s from %s (%s) - %s". PHP_EOL,
                    $d->ID,
                    $d->Title,
                    $d->Parent()->Title,
                    $customer->Title,
                    $d->DropboxID
                );

                $d->Archived = 1;
                $d->write();
            }
        }

        // if a folder has no files from the import then mark that as archived
        $folders = CustomerFolder::get()->filter([
            'CustomerID' => $customer->ID,
            'Archived' => '0'
        ])->exclude([
            'ID' => array_keys($this->active)
        ]);

        foreach ($folders as $folder) {
            echo "Archiving folder #". $folder->ID . " ". $folder->Title . " from ". $customer->Title . PHP_EOL;

            $folder->Archived = 1;
            $folder->write();
        }

        echo 'Clean up done for '. $customer->Title . PHP_EOL;

        echo 'Summary: Total Files: '. $totalFiles . PHP_EOL;
    }

    /**
     * @param DropboxCollection $contents
     * @param Customer $customer
     * @param CustomerFolder $customerFolder
     */
    public function importItems($contents, $customer, $customerFolder = null)
    {
        foreach ($contents->getItems()->all() as $item) {
            if ($item instanceof FolderMetadata) {
                // create or manage the folder;
                $existingFolder = null;

                if ($item->getName() === '.tag') {
                    continue;
                }

                $existingFolder = CustomerFolder::get()->filter([
                    'CustomerID' => $customer->ID,
                    'DropboxId:case' => $item->id
                ]);

                $existingFolder = $existingFolder->first();

                if (!$existingFolder) {
                    $existingFolder = CustomerFolder::create();
                    $existingFolder->CustomerID = $customer->ID;
                    $existingFolder->DropboxId = $item->id;
                }

                $existingFolder->Title = $item->getName();
                $existingFolder->Archived = 0;
                $existingFolder->ParentID = ($customerFolder) ? $customerFolder->ID : 0;
                $existingFolder->Sort = $this->folderSort;

                $this->folderSort += 1;

                $existingFolder->write();

                $this->markFolderActive($existingFolder->ID);

                $restoreObjSort = $this->objSort;
                $restoreFolderSort = $this->folderSort;

                try {
                    $contents = $this->dropbox->listFolder($item->getPathDisplay());

                    $this->folderSort = 0;
                    $this->objSort = 0;

                    $this->importItems($contents, $customer, $existingFolder);
                } catch (Exception $e) {
                    Injector::inst()->get(LoggerInterface::class)->error($e->getMessage(), [
                        'exception' => $e
                    ]);
                }

                $this->objSort = $restoreObjSort;
                $this->folderSort = $restoreFolderSort;

                // don't do file tasks.
                continue;
            }

            if ($customerFolder) {
                $folderTitle = $customerFolder->Title;
                $folderId = $customerFolder->ID;
            } else {
                $folderTitle = 'Root';
                $folderId = 0;
            }

            $this->markFileActive($folderId, $item);

            $obj = CustomerFile::get()->filter([
                "DropboxID:case" => $item->id,
                "CustomerID" => $customer->ID
            ])->first();

            if (!$obj) {
                $obj = CustomerFile::create();
                $obj->DropboxID = $item->id;
                $obj->Archived = 0;
                echo "Importing ". $item->id . " into customer ". $customer->Title . ' folder: '. $folderTitle . PHP_EOL;
            }

            try {
                $obj->ParentID = $folderId;
                $obj->CustomerID = $customer->ID;
                $obj->Title = $item->name;
                $obj->Archived = 0;
                $obj->Sort = $this->objSort;

                $this->objSort++;

                $obj->Size = $item->size;
                $obj->Path = $item->path_lower;
                $obj->ModifiedDate = $item->client_modified;
                $obj->Metadata = serialize($item);

                if ($obj->isChanged('ModifiedDate') || !$obj->isInDB())  {
                    $obj->write();
                }

                $this->saveThumbnail($obj, 'large');

                echo "Done #". $obj->ID ." \xE2\x9C\x85 ". $obj->Path . PHP_EOL;
            } catch (Exception $e) {
                Injector::inst()->get(LoggerInterface::class)->error($e->getMessage(), [
                    'exception' => $e
                ]);

                echo PHP_EOL. "\033[31m [WARNING] ". $e->getMessage() ."\033[0m". PHP_EOL;
            }
        }

        // If more items are available
        if ($contents->hasMoreItems()) {
            $cursor = $contents->getCursor();
            $listFolderContinue = $this->dropbox->listFolderContinue($cursor);

            $this->importItems($listFolderContinue, $customer, $customerFolder);
        }

    }

    /**
     * Tracks that an item in the folder is active still so it isn't cleaned up.
     * by the delete script.
     */
    public function markFileActive($folderId, $item)
    {
        $this->markFolderActive($folderId);

        $this->active[$folderId][] = $item->id;
    }

    public function markFolderActive($folderId)
    {
        if (!array_key_exists($folderId, $this->active)) {
            $this->active[$folderId] = [];
        }
    }

    /**
     * @param CustomerFile $obj
     * @param String $size
     */
    public function saveThumbnail($obj, $size = 'large')
    {
        $format = 'jpeg';

        try {
            $file = $this->dropbox->getThumbnail($obj->Path, $size, $format);
            $contents = $file->getContents();
            $folder = Controller::join_links(ASSETS_PATH, $obj->getThumbnailFilename($size));

            file_put_contents($folder , $contents);
        } catch (Exception $e) {
            Injector::inst()->get(LoggerInterface::class)->error($e->getMessage(), [
                'exception' => $e
            ]);
        }
    }

    public function getTemporaryLink($path)
    {
        return $this->dropbox->getTemporaryLink($path)->getLink();
    }

    public function syncLatestChanges()
    {
        $cursor = $this->dropbox->listFolderLatestCursor('/', [
            'include_deleted' => true
        ]);

        $changes = $this->dropbox->listFolderContinue($cursor);


        return false;
    }
}
