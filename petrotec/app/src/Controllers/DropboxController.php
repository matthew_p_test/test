<?php

use SilverStripe\Control\Controller;
use SilverStripe\SiteConfig\SiteConfig;

class DropboxController extends Controller
{
    private static $allowed_actions = [
		'callback',
		'download'
    ];

    public function callback()
    {
        if (isset($_GET['code']) && isset($_GET['state'])) {
            //Bad practice! No input sanitization!
            $code = $_GET['code'];
            $state = $_GET['state'];

            $manager = DropboxManager::inst();
            $manager->getAuthHelper()->getPersistentDataStore()->set('state', filter_var($_GET['state'], FILTER_SANITIZE_STRING));

            //Fetch the AccessToken
            $accessToken = $manager->getAccessToken($code, $state);

            $config = SiteConfig::current_site_config();
            $config->DropboxAccessToken = $accessToken->getToken();
            $config->write();

            return $this->redirect('admin/settings#Root_Dropbox');
        }
	}

	public function download()
	{
		$fileId = $this->request->param('ID');

		if (!$fileId) {
			return $this->httpError(400);
		}

		$file = CustomerFile::get()->byId($fileId);

		if (!$file || !$file->canView()) {
			return $this->httpError(404);
		}

		$link = $file->DownloadLink();

		if ($link) {
			return $this->redirect($link);
		} else {
			return 'AN error occured';
		}
	}
}
