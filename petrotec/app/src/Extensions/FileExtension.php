<?php

use SilverStripe\ORM\DataExtension;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\FieldList;
use SilverStripe\Security\Security;
use SilverStripe\Security\Permission;

class FileExtension extends DataExtension
{
	private static $db = [
		'Description' => 'Text'
	];

	public function updateCMSFields(FieldList $fields)
	{
		$fields->push(LiteralField::create('Link', sprintf('<a href="%s">%s</a>',
			$this->owner->AbsoluteLink,
			$this->owner->AbsoluteLink
		)));

		$fields->push(TextareaField::create('Description'));

		return $fields;
	}

	public function PortalPage()
	{
		return CustomerPortal::get()->first();
	}

	public function Bookmarked()
	{
		$member = Security::getCurrentUser();

		if ($member && $member->BookmarkedFiles()->find('ID', $this->owner->ID)) {
			return true;
		}

		return false;
	}
}
