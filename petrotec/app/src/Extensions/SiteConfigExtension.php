<?php

use SilverStripe\ORM\DataExtension;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\TextAreaField;
use SilverStripe\Forms\PasswordField;
use SilverStripe\Forms\LiteralField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RelationEditor;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use Kunnu\Dropbox\Dropbox;
use Kunnu\Dropbox\DropboxApp;
use SilverStripe\Control\Director;
use SilverStripe\Control\Controller;
use SilverStripe\Core\Config\Config;

class SiteConfigExtension extends DataExtension 
{
    private static $db = [
        'Phone' => 'Varchar',
        'Fax' => 'Varchar',
        'Email' => 'Varchar(255)',
        'Address' => 'Varchar(255)',
        'POBox' => 'Varchar(255)',
        'GaCode' => 'Varchar(255)',
        'DropboxAccessToken' => 'Varchar(255)'
    ];

    private static $has_one = [
        'HeaderLogo' => 'SilverStripe\Assets\Image'
    ];

    public function updateCMSFields(FieldList $fields)
    {
        $fields->addFieldsToTab('Root.Main', [
            UploadField::create('HeaderLogo', 'Logo'),
            TextField::create('GaCode', 'Google Analytics')
        ]);

        $fields->addFieldsToTab('Root.Categories', [
            GridField::create('ProjectCategories', 'Categories', ProjectCategory::get(), new GridFieldConfig_RelationEditor())
        ]);

        $fields->addFieldsToTab('Root.Footer', [
            TextField::create('Phone'),
            TextField::create('Fax'),
            TextField::create('Email'),
            TextField::create('Address'),
            TextField::create('POBox'),
            UploadField::create('FooterLogo', 'Logo')
        ]);

        $app = new DropboxApp(
            Config::inst()->get('Dropbox', 'AppKey'), 
            Config::inst()->get('Dropbox', 'AppSecret')
        );

        $dropbox = new Dropbox($app);
        $authHelper = $dropbox->getAuthHelper();
        $callbackUrl = Controller::join_links(Director::absoluteBaseURL(), "dropbox/callback/");
        $authHelper->getAuthUrl($callbackUrl);
        $fields->addFieldsToTab('Root.Dropbox', [
            TextField::create('DropboxAccessToken'),
            LiteralField::create('LinkDropbox', sprintf('<a href="%s" target="_blank">Link Dropbox</a> calls back to %s', 
                $authHelper->getAuthUrl($callbackUrl),
                $callbackUrl
            ))
        ]);

        return $fields;
    }
}