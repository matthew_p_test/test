<?php

use SilverStripe\ORM\DataExtension;
use SilverStripe\Security\Permission;

class MemberExtension extends DataExtension
{
	private static $db = [
		'LastLoggedIn' => 'Datetime'
	];

	private static $belongs_many_many = [
		'Customers' => 'Customer'
	];
	
	private static $many_many = [
		'BookmarkedFiles' => 'SilverStripe\Assets\File'
	];

	public function isAdmin()
	{
		return Permission::check('ADMIN');
	}

	public function beforeMemberLoggedIn()
	{
		$this->owner->LastLoggedIn = time();
	}
}