<?php

use SilverStripe\Core\Extension;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\TextareaField;

class AssetFormFactoryExtension extends Extension
{
	public function updateFormFields(FieldList $fields)
	{
		$fields->push(TextareaField::create('Description', 'Caption'));
	}
}