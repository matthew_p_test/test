<?php

use SilverStripe\Dev\BuildTask;

class SyncCandidateFolders extends BuildTask
{
    private static $segment = 'SyncCandidateFolders';

    protected $delete;

    protected $dropbox;

    public function run($request)
    {
        $this->dropbox = DropboxService::inst()->getDropboxHelper();

        $folders = $this->dropbox->listFolder("/");

        $this->delete = DropboxFolder::get()->map('ID', 'ID')->toArray();

        if ($request->getVar('clear')) {
            foreach ($this->delete as $delete) {
                $folder = DropboxFolder::get()->byId($delete);

                if ($folder) {
                    $folder->delete();
                }
            }
        }

        if ($folders) {
            foreach ($folders->getItems() as $folder) {
                $meta = $this->dropbox->getMetadata($folder->path_lower);
                $isFolder = $meta->getDataProperty('.tag') === 'folder';

                if ($isFolder) {
                    // skip git
                    if (strpos($folder->path_lower, '.git') !== false) {
                        continue;
                    }

                    $this->createFolder($folder);

                    $sub = $this->dropbox->listFolder($folder->path_lower);

                    if ($sub) {
                        foreach ($sub->getItems() as $child) {
                            $childIsFolder = $child->getDataProperty('.tag') === 'folder';

                            if ($childIsFolder) {
                                $this->createFolder($child);
                            }
                        }
                    }
                }
            }
        }

        if ($this->delete) {
            foreach ($this->delete as $delete) {
                $folder = DropboxFolder::get()->byId($delete);

                if ($folder) {
                    $folder->delete();
                }
            }
        }

        echo 'Done.';
    }

    public function createFolder($folder, $title = null)
    {
        $existing = DropboxFolder::get()->filter([
            'DropboxId' => $folder->id
        ])->first();

        if (!$existing) {
            $existing = DropboxFolder::create();
            $existing->DropboxId = $folder->id;
        }

        $existing->Path = $folder->path_lower;
        $existing->Title = ($title) ? $title : $folder->path_display;
        $existing->write();

        if (isset($this->delete[$existing->ID])) {
            unset($this->delete[$existing->ID]);
        }

        return $folder;
    }
}
