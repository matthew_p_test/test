<?php

use SilverStripe\Dev\BuildTask;
use SilverStripe\ORM\DB;

class SyncDropbox extends BuildTask
{
    private static $segment = 'SyncDropbox';

    public function run($request)
    {
        if ($request->getVar('latest')) {
            $service = DropboxService::inst();
            $service->syncLatestChanges();

            echo 'Done.';
            return;
        }

        if ($request->getVar('customerID')) {
            $cId = $request->getVar('customerID');

            $customers = Customer::get()->filter('ID', $cId);

            if ($request->getVar('clear')) {
                DB::query(sprintf(
                    'DELETE FROM CustomerFile WHERE CustomerID = %s',
                    $cId
                ));

                DB::query(sprintf(
                    'DELETE FROM CustomerFolder WHERE CustomerID = %s',
                    $cId
                ));
            }
        } else {
            $customers = Customer::get();

            if ($request->getVar('clear')) {
                DB::query('TRUNCATE TABLE CustomerFile');
                DB::query('TRUNCATE TABLE CustomerFolder');
            }
        }

        $start = time();

        foreach ($customers as $c) {
            echo "Syncing customer ". $c . ' from folder '. $c->DropboxFolder . PHP_EOL;

            $isSyncing = DB::query(sprintf('SELECT Syncing FROM Customer WHERE ID = %s', $c->ID))->value();

            if ($isSyncing && !$request->getVar('force')) {
                echo 'Skipping - sync in progress. Use ?force=1 to force' . PHP_EOL;
                continue;
            }

            $c->doSync();
        }

        $end = time();

        echo 'Ends. Finished in '. number_format(($end - $start), 0) . ' seconds';
    }

}
