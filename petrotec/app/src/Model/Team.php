<?php

use SilverStripe\ORM\DataObject;

class Team extends DataObject {

	private static $db = [
		"TeamName" => "Varchar(255)",
		"TeamTitle" => "Varchar(255)",
		"TeamEmail" => "Varchar(255)",
		"TeamPhone" => "Varchar(255)",
		"TeamMobile" => "Varchar(255)",
		"TeamText" => "HTMLText",
		"Sort" => 'Int'
	];

	private static $has_one = [
		"TeamPage" => "TeamPage",
		"TeamImage" => "SilverStripe\Assets\Image"
	];

	private static $summary_fields = [
		'TeamName' => 'Name',
		'TeamTitle' => 'Title',
		'TeamEmail' => 'Email'
	];

	private static $owns = [
		'TeamImage'
	];

	private static $default_sort = 'Sort ASC';

	public function onAfterWrite()
	{
		parent::onAfterWrite();

		$this->TeamImage()->publishRecursive();
	}
}