<?php

use SilverStripe\ORM\DataObject;
use SilverStripe\Control\Email\Email;
use SilverStripe\View\ArrayData;

class ContactPageEnquiry extends DataObject 
{
	private static $db = [
		'Name' => 'Varchar(255)',
		'Company' => 'Varchar(255)',
		'Phone' => 'Varchar(255)',
		'Email' => 'Varchar(255)',
		'Enquiry' => 'Text'
	];	

	public function onAfterWrite()
	{
		parent::onAfterWrite();
		
		$email = new Email();
		$email->setTo('info@petrotec.co.nz');
		$email->setSubject('Website Enquiry');
		$email->setData(new ArrayData([
			'Enquiry' => $this
		]));

		$email->setReplyTo($this->Email);
		$email->setHTMLTemplate('Email\\ContactPageEnquiry');

		$email->send();
	}
}