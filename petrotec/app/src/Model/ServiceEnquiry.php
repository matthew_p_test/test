<?php

use SilverStripe\Assets\File;
use SilverStripe\ORM\DataObject;
use SilverStripe\ORM\FieldType\DBField;
use SilverStripe\Control\Email\Email;
use SilverStripe\Control\Director;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\HeaderField;
use SilverStripe\Forms\FieldGroup;

class ServiceEnquiry extends DataObject
{
    private static $db = [
        'SiteName' => 'Varchar(255)',
        'Reference' => 'Varchar(255)',
        'Address' => 'Text',
        'SiteOwner' => 'Varchar(255)',
        'SiteOwnerPhone' => 'Varchar(255)',
        'SiteOwnerEmail' => 'Varchar(255)',
        'SiteManager' => 'Varchar(255)',
        'SiteManagerPhone' => 'Varchar(255)',
        'SiteManagerEmail' => 'Varchar(255)',
        'PumpInformation' => 'Text',
        'TankInformation' => 'Text',
        'OutdoorPayment' => 'Enum("Yes, No")',
        'CanopyLights' => 'Enum("LED, Bulb, N/A")',
        'MainID' => 'Enum("Yes, No")',
        'PriceSign' => 'Enum("Yes, No")',
        'Comments' => 'Text',
        'TMUExpiryDate' => 'Varchar',
        'PumpType' => 'Varchar'
    ];

    private static $has_one = [
        'Parent' => 'Page',
        'LocationCertificatePhoto' => File::class,
        'StationaryPhoto' => File::class
	];

	private static $cascade_deletes = [
		'LocationCertificatePhoto',
		'StationaryPhoto'
	];

    private static $default_sort = 'Created DESC';

    private static $summary_fields = [
        'Created',
        'Reference',
        'SiteName',
        'Address',
        'SiteOwner',
        'SiteOwnerPhone',
        'SiteOwnerEmail'
    ];

    protected $styles = 'width: 100%; margin-bottom: 20px;';

    public function onBeforeWrite()
    {
        parent::onBeforeWrite();

        if (!$this->Reference) {
            $this->Reference = strtoupper(substr(md5(time() . '-'. $this->SiteName), 0, 6));
        }
    }

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->replaceField('PumpInformation', FieldGroup::create(new LiteralField('PumpInformationAsTable', $this->PumpInformationAsTable())));

        $fields->replaceField('TankInformation', FieldGroup::create(new LiteralField('TankInformationAsTable', $this->TankInformationAsTable())));

        return $fields;
    }

    public function sendToAdmin()
    {
        $email = Email::create();
        $email->setSubject('Service Form Submission');
        $email->setTo('info@petrotec.co.nz');
        $email->setHTMLTemplate('Email/ServiceFormSubmission');
        $email->setData([
            'Enquiry' => $this
        ]);

        if ($this->LocationCertificatePhoto()->exists()) {
            $this->LocationCertificatePhoto()->grantFile();
            $this->LocationCertificatePhoto()->publishFile();

            $path = str_replace(Director::absoluteBaseURL(), '', $this->LocationCertificatePhoto()->getAbsoluteURL());
            $email->addAttachment(BASE_PATH . '/'.$path);
        }

        if ($this->StationaryPhoto()->exists()) {
            $this->StationaryPhoto()->grantFile();
            $this->StationaryPhoto()->publishFile();

            $path = str_replace(Director::absoluteBaseURL(), '', $this->StationaryPhoto()->getAbsoluteURL());
            $email->addAttachment(BASE_PATH . '/'.$path);
        }

        $email->send();
    }

    public function PumpInformationAsTable()
    {
        $output = '<table style="'. $this->styles .'"><thead><tr><th>Pump #</th><th>Model</th><th>Nozzles</th><th>Grades</th><th>Notes</th></tr></thead><tbody>';

        $this->PumpInformation = trim((string) $this->PumpInformation);

        if ($this->PumpInformation) {
            $pumpData = json_decode($this->PumpInformation, true);
            $data = [];

            foreach ($pumpData as $col => $values) {
                foreach ($values as $k => $v) {
                    if (!isset($data[$k])) {
                        $data[$k] = [];
                    }

                    if (is_array($v)) {
                        $v = implode(', ', $v);
                    }

                    $data[$k][$col] = $v;
                }
            }

            foreach ($data as $pump) {
                $pump = (object) $pump;

                $output .= sprintf(
                    '<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>',
                    (isset($pump->PumpNo)) ? $pump->PumpNo : '',
                    (isset($pump->Model)) ? $pump->Model : '',
                    (isset($pump->Nozzles)) ? $pump->Nozzles : '',
                    (isset($pump->Grades)) ? $pump->Grades : '',
                    (isset($pump->Notes)) ? $pump->Notes : ''
                );
            }
        } else {
            $output .= '<tr><td colspan="5">No Pump Data Provided</td></tr>';
        }
        $output .= "</tbody></table>";

        return DBField::create_field('HTMLText', $output);
    }

    public function TankInformationAsTable()
    {
        $output = '<table style="'. $this->styles .'"><thead><tr><th>Size</th><th>Grade</th><th>Location</th><th>Fuel Monitoring</th><th>Brand</th></tr></thead><tbody>';

        $this->TankInformation = trim((string) $this->TankInformation);

        if ($this->TankInformation) {
            $tankData = json_decode($this->TankInformation, true);
            $data = [];

            foreach ($tankData as $col => $values) {
                foreach ($values as $k => $v) {
                    if (!isset($data[$k])) {
                        $data[$k] = [];
                    }

                    if (is_array($v)) {
                        $v = implode(', ', $v);
                    }

                    $data[$k][$col] = $v;
                }
            }

            foreach ($data as $tank) {
                $tank = (object) $tank;

                $output .= sprintf(
                    '<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>',
                    (isset($tank->Size)) ? $tank->Size : '',
                    (isset($tank->Grade)) ? $tank->Grade : '',
                    (isset($tank->Location)) ? $tank->Location : '',
                    (isset($tank->FuelMonitoring)) ? $tank->FuelMonitoring : '',
                    (isset($tank->Brand)) ? $tank->Brand : ''
                );
            }
        } else {
            $output .= '<tr><td colspan="5">No Tank Data Provided</td></tr>';
        }

        $output .= "</tbody></table>";

        return DBField::create_field('HTMLText', $output);
    }

    public function canCreate($member = null, $context = [])
    {
        return false;
    }
}
