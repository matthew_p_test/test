<?php

use SilverStripe\Control\Controller;
use SilverStripe\Control\Director;
use SilverStripe\ORM\DataObject;
use SilverStripe\Security\Permission;

class CustomerFile extends DataObject
{
    private static $db = [
        'Title' => 'Varchar(255)',
        'Metadata' => 'Text',
        'Archived' => 'Boolean',
        'Size' => 'Int',
        'Path' => 'Varchar(255)',
        'Sort' => 'Int',
        'ModifiedDate' => 'Datetime',
        'DropboxID' => 'Varchar(255)',
        'DownloadLink' => 'Varchar(255)',
        'DownloadLinkExpires' => 'Datetime'
    ];

    private static $has_one = [
        'Parent' => 'CustomerFolder',
        'Customer' => 'Customer'
    ];

    private static $default_sort = 'Title ASC';

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        return $fields;
    }

    public function canView($member = null)
    {
        if ($this->Archived) {
            return false;
		}

        return $this->Customer()->canView($member) || $this->Parent()->canView($member);
    }

    public function canEdit($member = null)
    {
        return Permission::checkMember($member, 'ADMIN');
    }

    public function canDelete($member = null)
    {
        return $this->canEdit($member);
    }

    public function getThumbnail($size = 'large')
    {
        if (!$this->hasThumbnail($size)) {
            try {
                DropboxService::inst()->saveThumbnail($this, $size);
            } catch (Exception $e) {
                return false;
            }
        }

        return Controller::join_links(
            Director::absoluteBaseURL(),
            'assets',
            $this->getThumbnailFilename($size)
        );
    }

    public function getThumbnailFilename($size = 'large')
    {
        $hash = md5($this->ID . '-'. $this->dbObject('Created')->getTimestamp());

        return "_thumbs-" . $hash . '-'. $size .'.jpeg';
    }

    public function hasThumbnail($size = 'large')
    {
        $name = $this->getThumbnailFilename($size);

        return file_exists(Controller::join_links(ASSETS_PATH, $name));
	}

	public function LocalDownloadLink()
	{
		return Controller::join_links(Director::absoluteBaseURL(), 'dropbox/download', $this->ID);
	}

	public function Extension()
	{
		$ext = substr($this->Path, strrpos($this->Path, '.') + 1);

		return strtolower($ext);
	}

	public function DownloadLink()
    {
        if ($this->DownloadLink && $this->dbObject('DownloadLinkExpires')->getTimestamp() > time()) {
            return $this->DownloadLink;
        }

        try {
            $this->DownloadLink = DropboxService::inst()->getTemporaryLink($this->Path);
            $this->dbObject('DownloadLinkExpires')->setValue(strtotime("+2 hours"));
            $this->write();

            return $this->DownloadLink;
        } catch (Exception $e) {

        }

        return '';
    }
}
