<?php

use Psr\Log\LoggerInterface;
use SilverStripe\ORM\DataObject;
use SilverStripe\Security\Member;
use SilverStripe\ORM\FieldType\DBField;
use SilverStripe\Assets\Folder;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RelationEditor;
use SilverStripe\Security\Permission;
use SilverStripe\Security\Security;
use SilverStripe\View\Parsers\URLSegmentFilter;

class Customer extends DataObject
{
    private static $db = [
		'Title' => 'Varchar(255)',
		'Key' => 'Varchar(255)',
        'Syncing' => 'Boolean',
        'LastSynced' => 'Datetime',
        'DropboxFolder' => 'Varchar(255)'
    ];

    private static $many_many = [
        'Logins' => Member::class,
        'Files' => CustomerFile::class,
        'Folders' => CustomerFolder::class
    ];

    private static $summary_fields = [
        'Title' => 'Title'
    ];

    private static $owns = [
        'Files'
	];

	private static $cascade_deletes = [
		'Files',
		'Folders'
	];

    public function getCMSFields()
    {
        $folders = DropboxService::inst()->getFolders();

        $fields = parent::getCMSFields();
        $fields->removeByName('Files');
        $fields->removeByName('Folders');
        $fields->removeByName('Logins');
        $fields->addFieldToTab('Root.Main', UploadField::create('Files'));
        $fields->addFieldToTab('Root.Main', GridField::create('Logins', 'Logins', $this->Logins(), new GridFieldConfig_RelationEditor()));
        $fields->addFieldToTab('Root.Main', GridField::create('Folders', 'Folders', $this->Folders(), new GridFieldConfig_RelationEditor()));
        $fields->addFieldToTab('Root.Main', DropdownField::create('DropboxFolder', 'Link to Dropbox', $folders, 'No Dropbox folder selected'));

        return $fields;
	}

	public function onBeforeWrite()
    {
        parent::onBeforeWrite();

        $filter = URLSegmentFilter::create();

        $this->Key = $filter->filter($this->Title);
    }

    public function LastLoggedIn()
    {
        if ($this->Logins()->exists()) {
            return $this->Logins()->sort('LastLoggedIn', 'DESC')->first()->dbObject('LastLoggedIn');
        }

        return DBField::create_field('Varchar', 'Never');
	}

	public function canView($member = null)
	{
		if (!$member) {
			$member = Security::getCurrentUser();
		}

		if (!$member) {
			return false;
		}

		return (Permission::check('ADMIN') || $this->Logins()->filter('ID', $member->ID)->exists());
	}

	public function doSync()
    {
        $this->Syncing = true;
        $this->LastSynced = time();
        $this->write();

        try {
            if ($this->DropboxFolder) {
                $inst = DropboxService::inst();
                $inst->syncFilesToFolder($this->DropboxFolder, $this);
            }
        } catch (Exception $e) {
            Injector::inst()->get(LoggerInterface::class)->error($e);
        }

        $this->Syncing = false;
        $this->LastSynced = time();
        $this->write();
    }
}
