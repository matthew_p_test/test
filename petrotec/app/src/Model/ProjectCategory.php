<?php

use SilverStripe\ORM\DataObject;

class ProjectCategory extends DataObject
{
    private static $db = [
        'Title' => 'Varchar(255)',
        'Sort' => 'Int'
    ];

    private static $belongs_many_many = [
        'Projects' => 'ProjectPage'
    ];

    private static $default_sort = 'Title ASC';

    private static $summary_fields = [
        'Title',
        'Projects.Count'
    ];
}