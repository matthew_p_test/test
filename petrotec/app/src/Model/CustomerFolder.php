<?php

use SilverStripe\ORM\DataObject;

class CustomerFolder extends DataObject
{
    private static $db = [
        'Title' => 'Varchar(255)',
        'Archived' => 'Boolean',
        'Sort' => 'Int',
        'DropboxId' => 'Varchar(255)'
    ];

    private static $has_one = [
        'Customer' => 'Customer',
        'Parent' => 'CustomerFolder'
    ];

    private static $has_many = [
        'Files' => 'CustomerFile',
        'Children' => 'CustomerFolder'
    ];

    private static $owns = [
        'Files',
        'Children'
    ];

    private static $cascade_deletes = [
        'Files',
        'Children'
    ];

    private static $summary_fields = [
        'Title' => 'Title',
        'ActiveFiles.Count' => 'Files'
    ];

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $files = $fields->dataFieldByName('Files');

        if ($files) {
            $files->getConfig()->getComponentByType(GridFieldDeleteAction::class)
                ->setRemoveRelation(false);
        }

        return $fields;
	}

	public function canView($member = null)
    {
        if ($this->Archived) {
            return false;
        }

		return $this->Customer()->canView($member) ||
			($this->Parent()->exists() && $this->Parent()->canView($member));
    }
}
