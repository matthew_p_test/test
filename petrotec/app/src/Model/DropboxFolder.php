<?php

use SilverStripe\ORM\DataObject;

class DropboxFolder extends DataObject
{
    private static $db = [
        'DropboxId' => 'Varchar(255)',
        'Path' => 'Varchar(255)',
        'Title' => 'Varchar(255)'
    ];
}
