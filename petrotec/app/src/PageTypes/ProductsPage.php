<?php

use SilverStripe\Forms\CheckboxField;
use SilverStripe\Control\Controller;
use SilverStripe\Control\Director;

class ProductsPage extends Page 
{
	private static $db = [

	];
	
	private static $allowed_children = ['ProductPage'];

	private static $default_child = 'ProductPage';
	
	public function getCMSFields() 
	{
        $fields = parent::getCMSFields();
		
		return $fields;
	}
}

class ProductsPage_Controller extends PageController {

}