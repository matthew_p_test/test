<?php

use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\TextField;
use SilverStripe\Control\Controller;
use SilverStripe\Control\Director;
use SilverStripe\AssetAdmin\Forms\UploadField;

class ProductPage extends Page 
{
    private static $db = [
        'Icon' => 'Varchar(255)',
        'Feature' => 'Boolean'
    ];
    
    private static $has_one = [
        'GreyLogo' => 'SilverStripe\Assets\Image',
        "Image" => "SilverStripe\Assets\Image",
        "Brochure" => "SilverStripe\Assets\File"
    ];
    
    private static $owns = [
        'Image',
        'GreyLogo',
        'Brochure'
    ];
    
    public function getCMSFields() 
    {
        $fields = parent::getCMSFields();
        
        $fields->addFieldsToTab('Root.Main', [
            new TextField('Icon'),
            new CheckboxField('Feature'),
            new UploadField('Image', 'Product Image'),
            new UploadField('Brochure', 'Product Brochure'),
            new UploadField('GreyLogo', 'Product Logo')
        ]);
        
        return $fields;
    }
}

class ProductPage_Controller extends PageController 
{
    public function init() 
    {
        parent::init();

        if ($this->BrochureID) {
            return $this->redirect($this->Brochure()->getURL());
        }
    }
}