<?php

use SilverStripe\Forms\CheckboxSetField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use Bummzack\SortableFile\Forms\SortableUploadField;
use SilverStripe\ORM\ArrayList;
use SilverStripe\Assets\Image;

class ProjectPage extends Page
{
	private static $many_many = [
		'Photos' => 'SilverStripe\Assets\Image',
		'Related' => 'ServicePage',
		'Categories' => 'ProjectCategory'
	];

	private static $many_many_extraFields = [
		'Photos' => [
			'SortOrder' => 'Int'
		]
	];

	private static $owns = [
		'Photos'
	];

	public function getCMSFields()
	{
		$fields = parent::getCMSFields();

		$fields->addFieldToTab('Root.Main', SortableUploadField::create('Photos', 'Photos')->setSortColumn('SortOrder'));
		$fields->addFieldToTab('Root.Main', CheckboxSetField::create('Categories', 'Related Categories', ProjectCategory::get()->map()));
		$fields->addFieldToTab('Root.Main', CheckboxSetField::create('Related', 'Related Services', ServicePage::get()->map()));
		return $fields;
	}
	
	public function SortedPhotos()
	{
		return $this->Photos()->Sort('SortOrder');
	}
}

class ProjectPage_Controller extends PageController
{

}