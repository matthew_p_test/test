<?php

use SilverStripe\Forms\TextField;
use SilverStripe\AssetAdmin\Forms\UploadField;

class ServicePage extends Page
{

	private static $db = [
		'Icon' => 'Varchar(255)'
	];

	private static $has_one = [
		'Image' => 'SilverStripe\Assets\Image'
	];

    private static $owns = [
        'Image'
    ];

    private static $belongs_many_many = [
        'Projects' => 'ProjectPage'
    ];

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        $fields->addFieldToTab('Root.Main', TextField::create('Icon'));
        $fields->addFieldToTab('Root.Main', new UploadField('Image', 'Service Image'));

        return $fields;
    }

    public function getRecentProjects()
    {
        return $this->Projects()->sort('Created', 'DESC')->limit(4);
    }
}