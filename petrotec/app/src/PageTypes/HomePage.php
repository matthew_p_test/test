<?php

use SilverStripe\Forms\TabSet;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\TreeDropdownField;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Forms\GridField\GridFieldConfig_RelationEditor;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\AssetAdmin\Forms\UploadField;

class HomePage extends Page 
{
	private static $db = [
		'IntroTitle' => 'Varchar(255)',
		'InlineBannerImage' => 'Boolean',
		'HideBoxes' => 'Boolean',
		'FeaturedProjectCaption' => 'HTMLText'
	];

	private static $has_one = [
		'FeaturedProjectImage' => 'SilverStripe\Assets\Image'
	];

	private static $owns = [
		'FeaturedProjectImage'
	];

	private static $many_many = [
		'FeaturedProjects' => 'ProjectPage'
	];

	private static $many_many_extraFields = [
		'FeaturedProjects' => [
			'HomeOrder' => 'Int'
		]
	];

	private static $allowed_children = [
        'SectionsPage'
    ];
	
	public function getCMSFields() 
	{
		$fields = parent::getCMSFields();

		$fields->addFieldsToTab('Root.Main', [
			new TextField('IntroTitle'),
			new CheckboxField('HideBoxes'),
			new CheckboxField('InlineBannerImage')
		]);

		$fields->removeByName('Content');
		$config = GridFieldConfig_RelationEditor::create();

		$projects = GridField::create('FeaturedProjects', 'Featured Projects', $this->FeaturedProjects(), $config);
		$fields->addFieldsToTab('Root.Main', [
			UploadField::create('FeaturedProjectImage', 'Featured Project Image'),
			HtmlEditorField::create('FeaturedProjectCaption'),

			$projects
		]);
		
		return $fields;
	}
}


class HomePage_Controller extends PageController
{

	public function BannerImages()
    {
		return BannerImage::get();
	}
}