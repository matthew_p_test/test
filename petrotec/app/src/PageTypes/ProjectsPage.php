<?php

use Symbiote\GridFieldExtensions\GridFieldOrderableRows;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RelationEditor;
use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\CheckboxSetField;
use SilverStripe\Forms\Form;
use SilverStripe\Forms\FormAction;
use SilverStripe\ORM\DB;
use SilverStripe\ORM\ArrayList;

class ProjectsPage extends Page 
{
	private static $allowed_children = [
		'ProjectPage'
	];

	private static $default_child = 'ProjectPage';

	public function getCMSFields()
	{
		$fields = parent::getCMSFields();

		return $fields;
	}

	public function getServices()
	{
		return ServicePage::get();
	}

	public function getCategories()
	{
		return ProjectCategory::get();
	}

	public function Children()
	{
		$children = parent::Children();

		return $children->exclude('ClassName', 'ProjectPage');
	}
}

class ProjectsPage_Controller extends PageController 
{
	public function FilterForm()
	{
		$fields = new FieldList(
			CheckboxSetField::create('Categories', 'Filter by category', $this->getCategories()),
			CheckboxSetField::create('Services', 'Filter by service', $this->getServices())
		);

		$actions = new FieldList(
			FormAction::create('doFilter', 'FIlter')
		);

		return new Form($this, 'FilterForm', $fields, $actions);
	}
}