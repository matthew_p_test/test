<?php

use Symbiote\GridFieldExtensions\GridFieldOrderableRows;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RelationEditor;

class TeamPage extends Page {

	private static $has_many = [
		"Team" => "Team"
	];
	
	private static $allowed_children = [];

	public function getCMSFields() {
        $fields = parent::getCMSFields();
		
		$fields->addFieldToTab('Root.Main', $grid = new GridField('Team', 'Team', $this->Team(), GridFieldConfig_RelationEditor::create()));
		$grid->getConfig()->addComponent(new GridFieldOrderableRows('Sort'));

		return $fields;
	}	
}

class TeamPage_Controller extends PageController {

}