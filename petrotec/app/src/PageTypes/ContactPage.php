<?php

use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\Form;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\EmailField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\FormAction;
use SilverStripe\Forms\RequiredFields;

class ContactPage extends Page {
    
    private static $db = [
        'HideLocations' => 'Boolean'
    ];

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        $fields->addFieldToTab('Root.Main', CheckboxField::create('HideLocations'));

        return $fields;
    }
}

class ContactPage_Controller extends PageController {
    
    private static $allowed_actions = [
        'index',
        'thanks',
        'Form'
    ];

    public function Form()
    {
        $fields = new FieldList(
            TextField::create('Name', 'Name'),
            TextField::create('Company', 'Company'),
            EmailField::create('Email', 'Email'),
            TextField::create('Phone', 'Phone'),
            TextareaField::create('Enquiry')
        );

        $actions = new FieldList(
            new FormAction('doForm', 'Send')
        );

        $validator = new RequiredFields([
            'Name', 'Email', 'Enquiry'
        ]);

        $form = new Form($this, __FUNCTION__, $fields, $actions, $validator);
        $form->addExtraClass('form-contact email-form');
        $form->enableSpamProtection();

        return $form;
    }

    public function doForm($data, $form)
    {
        $enquiry = new ContactPageEnquiry();

        $form->saveInto($enquiry);
        $enquiry->write();

        return $this->redirect($this->Link('thanks'));
    }

    public function thanks()
    {
        return [
            'Title' => 'Thanks for your enquiry',
            'Content' => 'Thank you, we have received your enquiry and will respond shortly.',
            'Form' => ''
        ];
    }
}

