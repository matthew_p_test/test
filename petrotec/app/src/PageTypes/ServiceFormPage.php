<?php

use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\Form;
use SilverStripe\ORM\FieldType\DBField;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\EmailField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\FormAction;
use SilverStripe\Forms\RequiredFields;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;

class ServiceFormPage extends Page {
	
	private static $db = [];

	private static $has_many = [
		'Submissions' => 'ServiceEnquiry'
	];

	public function getCMSFields()
	{
		$fields = parent::getCMSFields();

		$config = new GridFieldConfig_RecordEditor();
		
		$fields->addFieldToTab('Root.Submissions', 
			GridField::create('Submissions', 'Submissions', $this->Submissions(), $config)
		);

		return $fields;
	}
}

class ServiceFormPage_Controller extends PageController {
	
	private static $allowed_actions = [
		'index',
		'thanks',
		'Form'
	];

	public function Form()
	{		
		return new ServiceForm($this, __FUNCTION__);
	}

	public function thanks()
	{
		$ref = $this->getRequest()->param('ID');

		return [
			'Title' => 'Thanks for your enquiry',
			'Content' => DBField::create_field('HTMLText','<p>Thank you, we have received your enquiry <strong>#'. $ref . '</strong> and will respond shortly. If this is urgent please follow up your enquiry to 0800 477 134.</p>'),
			'Form' => '',
			'Reference' => $ref
		];
	}
}

