<?php

use SilverStripe\Forms\TextField;

class JobPage extends Page
{

	private static $db = [
		'Location' => 'Varchar(255)'
	];

	private static $defaults = [
		'ShowInMenus' => 0
	];

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        $fields->addFieldToTab('Root.Main', TextField::create('Location'));

        return $fields;
    }
}