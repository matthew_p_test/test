<?php

use SilverStripe\Forms\TextField;

class AboutPage extends Page
{
	private static $db = [];

	private static $defaults = [
		'ShowInMenus' => 0
	];

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        return $fields;
    }
}