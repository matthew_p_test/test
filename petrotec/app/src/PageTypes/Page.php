<?php

use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\CMS\Controllers\ContentController;
use SilverStripe\Forms\HtmlEditor\HtmlEditorField;
use SilverStripe\Assets\Image;
use SilverStripe\Control\Director;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Forms\TextField;
use SilverStripe\Security\Permission;
use SilverStripe\Subsites\Model\Subsite;
use SilverStripe\Core\Environment;
use Bummzack\SortableFile\Forms\SortableUploadField;

class Page extends SiteTree
{
	private static $db = [
		'IntroText' => 'Text',
		'BannerCaption' => 'HTMLText',
		'AfterGalleryContent' => 'HTMLText'
	];

	private static $has_one = [
		'BannerImage' => 'SilverStripe\Assets\Image'
	];

	private static $owns = [
		'BannerImage',
		'GalleryImages'
	];

	private static $many_many = [
		'GalleryImages' => Image::class
	];

	private static $many_many_extraFields = [
		'GalleryImages' => [
			'SortGallery' => 'Int'
		]
	];

	public function getCMSFields()
	{
		$fields = parent::getCMSFields();
		$fields->addFieldsToTab('Root.Main', [
			TextField::create('IntroText')
		]);

		$fields->addFieldsToTab('Root.Banner', [
			UploadField::create('BannerImage'),
			HtmlEditorField::create('BannerCaption')
		]);

		$fields->addFieldsToTab('Root.Gallery', [
			SortableUploadField::create('GalleryImages')->setSortColumn('SortGallery'),
			HtmlEditorField::create('AfterGalleryContent')
		]);

		return $fields;
	}

	public function Services()
	{
		return ServicePage::get();
	}

	public function ThemeChangeMarker()
	{
		return '?m='. (
			filemtime(BASE_PATH .'/themes/petrotec/scss/theme.scss')
		);
	}

	public function getSortedGalleryImages()
	{
		return $this->GalleryImages()->sort('Sort', 'ASC');
	}

	public function AppJSChangeMarker()
	{
		return '?m='. filemtime(BASE_PATH . '/app/dist/app.min.js');
	}

	public function ProjectPages()
	{
		return ProjectPage::get()->where('Title IS NOT NULL');
	}

	public function ProjectsPage()
	{
		return ProjectsPage::get()->first();
	}

	public function ProductsPage()
	{
		return ProductsPage::get()->first();
	}

	public function Products()
	{
		$products = ProductsPage::get()->first();

		return ($products) ? $products->AllChildren() : null;
	}

	public function ContactPage()
	{
		return ContactPage::get()->first();
	}

	public function PortalPage()
	{
		return CustomerPortal::get()->first();
	}

	public function Subsites()
	{
		return Subsite::all_sites(false);
	}

	public function Subsite()
	{
		return Subsite::currentSubsite();
	}
}

class PageController extends ContentController {

	private static $allowed_actions = [];

	protected function init()
	{
		parent::init();

		if (Environment::getEnv('SS_FORCE_SSL')) {
			if (!Director::is_https() && !$this->redirectedTo()) {
				Director::forceSSL();
			}
		}
	}

	public function IsAdmin()
	{
		return Permission::check('ADMIN');
	}
}
