<?php

use SilverStripe\Forms\TabSet;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\RequiredFields;
use SilverStripe\Forms\TextAreaField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\TreeDropdownField;
use SilverStripe\CMS\Model\SiteTree;

class ArticlePage extends Page {

	private static $db = [
		'Abstract' => 'Varchar(200)'
	];

	private static $has_one = [
		'Image' => 'SilverStripe\Assets\Image',
		'Link' => 'SilverStripe\CMS\Model\SiteTree'
	];

    private static $owns = [
        'Image'
    ];

    function getCMSValidator() {
		return new RequiredFields(['LinkID']);
	}

	public function getCMSFields() {
		$fields = parent::getCMSFields();
		$fields->removeFieldFromTab("Root.Content.Main","Content");
		$fields->addFieldsToTab('Root.Main', [
			TextAreaField::create('Abstract', 'Abstract to display on the tile (200 character limit)'),
			UploadField::create('Image', 'Image'),
			TreeDropdownField::create('LinkID', 'Page to link to', SiteTree::class, 'ID', 'MenuTitle')
		]);
		return $fields;
	}
}