<?php

use SilverStripe\Assets\File;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Security\Security;
use SilverStripe\Security\SecurityToken;
use SilverStripe\Control\Director;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\Form;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\OptionsetField;
use SilverStripe\Forms\FormAction;
use SilverStripe\Forms\GridField\GridFieldConfig_RelationEditor;
use SilverStripe\ORM\ArrayList;
use SilverStripe\Versioned\Versioned;

class CustomerPortal extends Page
{
    private static $many_many = [
        'Customers' => 'Customer'
    ];

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        $fields->removeByName('Content');
        $fields->addFieldsToTab('Root.Main', GridField::create('Customers', 'Customers', $this->Customers(), GridFieldConfig_RelationEditor::create()));

        return $fields;
    }

    public function Folders()
    {
        $member = Security::getCurrentUser();
        $folders = ArrayList::create();

        if ($member) {
            if ($member->Customers()->count() > 0) {

                foreach ($member->Customers() as $customer) {
                    $folders->merge(CustomerFolder::get()->filter([
                        'CustomerID' => $customer->ID,
                    ])->exclude([
						'Archived' => 1
					]));
                }
            }
        }

        return $folders;
    }

    public function Uploads()
    {
        $member = Security::getCurrentUser();
        $output = ArrayList::create();

        if ($member) {
            if ($member->Customers()->count() > 0) {
                foreach($member->Customers() as $customer) {
					foreach ($customer->Files()->exclude('Archived', 1) as $file) {
						$output->push($file);
					}
                }
            }
        }

        return $output;
    }
}

class CustomerPortal_Controller extends PageController
{
    private static $allowed_actions = [
        'bookmark'
    ];

    public function FilterForm()
    {
        $member = Security::getCurrentUser();

        $fields = new FieldList(
            new OptionsetField('Customer', 'Customer Portal', $member->Customers(), $member->Customers()->first()->ID)
        );

        $actions = new FieldList(
            new FormAction('doFilter', 'Save')
        );

        return new Form($this, __FUNCTION__, $fields, $actions);
    }

    public function bookmark($request)
    {
        $user = Security::getCurrentUser();

        if (!SecurityToken::inst()->checkRequest($request) || !$user || !$id = $request->param('ID')) {
            return $this->httpError(400);
        }

        $file = CustomerFile::get()->byId($id);

        if (!$file || !$file->canView()) {
            return $this->httpError(404);
        }

        if (!$user->BookmarkedFiles()->find('ID', $file->ID)) {
            $user->BookmarkedFiles()->add($file);
        } else {
            $user->BookmarkedFiles()->remove($file);
        }

        return $this->redirect($this->Link());
    }
}
