<?php

use SilverStripe\Forms\Form;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\CompositeField;
use SilverStripe\Forms\FieldGroup;
use SilverStripe\Forms\DateField;
use SilverStripe\Forms\RequiredFields;
use SilverStripe\Forms\EmailField;
use SilverStripe\Forms\FormAction;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\OptionsetField;
use SilverStripe\Forms\CheckboxSetField;
use SilverStripe\Forms\FileField;
use FullscreenInteractive\ManyField\ManyField;

class ServiceForm extends Form
{
	public function __construct($controller, $name)
	{
		$fields = new FieldList(
			CompositeField::create(
				TextField::create('SiteName', 'Site Name:'),
				TextareaField::create('Address', 'Site Address:'),
				FieldGroup::create(
					TextField::create('SiteOwner', 'Site Owner:'),
					TextField::create('SiteOwnerPhone', 'Contact No.'),
					EmailField::create('SiteOwnerEmail', 'Email:')
				),
				FieldGroup::create(
					TextField::create('SiteManager', 'Site Manager:'),
					TextField::create('SiteManagerPhone', 'Contact No.'),
					EmailField::create('SiteManagerEmail', 'Email:')
				)
			)->setLegend('Site Information')->setTag('fieldset'),
			CompositeField::create(
				ManyField::create('PumpInformation', [
					TextField::create('PumpNo', 'Pump #'),
					TextField::create('Model', 'Model'),
					TextField::create('Nozzles', 'Nozzles'),
					CheckboxSetField::create('Grades', 'Grades', [
						'91' => '91',
						'95' => '95',
						'Diesel' => 'Diesel'
					]),
					TextareaField::create('Notes', 'Notes')
				]),
				FieldGroup::create([
					DateField::create('TMUExpiryDate', 'TMU Expiry Date'),
					OptionsetField::create('PumpType', 'Pump Type', [
						'Suction' => 'Suction',
						'Pressure' => 'Pressure'
					])
				])
			)->setLegend('Pump / Dispenser Information')->setTag('fieldset'),
			CompositeField::create(
				ManyField::create('TankInformation', [
					TextField::create('Size', 'Size'),
					OptionsetField::create('Grade', 'Grade', [
						'91' => '91',
						'95' => '95',
						'Diesel' => 'Diesel'
					]),
					OptionsetField::create('Location', 'Location', [
						'Aboveground' => 'Aboveground',
						'Underground' => 'Underground'
					]),
					OptionsetField::create('FuelMonitoring', 'Fuel Monitoring', [
						'Yes' => 'Yes',
						'No' => 'No'
					]),
					TextField::create('Brand', 'Brand')
				])
			)->setLegend('Tank Information')->setTag('fieldset'),
			CompositeField::create(
				OptionsetField::create('OutdoorPaymentTerminal', 'Outdoor Payment Terminal (OPT)', [
					'Yes' => 'Yes',
					'No' => 'No'
				]),
				OptionsetField::create('CanopyLights', 'Canopy Lights', [
					'LED' => 'LED',
					'Bulb' => 'Bulb',
					'N/A' => 'N/A'
				]),
				OptionsetField::create('MainID', 'Main ID', [
					'Yes' => 'Yes',
					'No' => 'No'
				]),
				OptionsetField::create('PriceSign', 'Price Sign', [
					'Yes' => 'Yes',
					'No' => 'No'
				]),
				FileField::create('LocationCertificatePhoto', 'Photo of Location Certificates'),
				FileField::create('StationaryPhoto', 'Photo of Stationary Container')
			)->addExtraClass('option-list')->setLegend('General Site Information')->setTag('fieldset'),
			CompositeField::create(
				TextareaField::create('Comments', 'Notes / Comments')
			)->setLegend('Additional Notes')->setTag('fieldset')
		);

		$actions = new FieldList(
			FormAction::create('doServiceForm', 'Submit')
		);
		
		$required = new RequiredFields(
			'SiteName',
			'Address',
			'SiteManager',
			'SiteManagerPhone',
			'SiteManagerEmail',
			'SiteOwner',
			'SiteOwnerPhone',
			'SiteOwnerEmail'
		);

		parent::__construct($controller, $name, $fields, $actions, $required);
	}

	public function doServiceForm($data, $form)
	{
		$enquiry = new ServiceEnquiry();
		$form->saveInto($enquiry);

		$enquiry->ParentID = $this->controller->ID;
		$enquiry->write();
		$enquiry->sendToAdmin();

		return $this->controller->redirect(
			$this->controller->Link('thanks/'. $enquiry->Reference)
		);
	}
}